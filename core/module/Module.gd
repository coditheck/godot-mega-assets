"""
	@author: Obrymec
	@company: CodiTheck
	@famework: Godot Mega Assets
	@compatibility: Godot 3.x.x
	@platform: ANDROID || IOS || MACOSX || UWP || HTML5 || WINDOWS || LINUX
	@license: MIT
	@source: https://godot-mega-assets.herokuapp.com/home
	@language: GDscript
	@dimension: 2D || 3D
	@type: Module
	@version: 0.2.6
	@created: 2021-06-12
	@updated: 2022-03-19
"""
################################################################################### [Main class] ############################################################################
"""@Description: Module is a class that represents some basics functionalities common to all modules of the Godot Mega Assets framework."""
tool class_name Module, "module.svg" extends MegaAssets;

################################################################################### [Attributes] ############################################################################
# Contains all basics properties of a Godot Mega Assets module.
func _basics_module_properties () -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Contains the module title category.
		self.bind_prop (Dictionary ({title = "Module", index = 0}));
		"""@Description: Checks the operating status of a module (ON/OFF)."""
		self.bind_prop (Dictionary ({source = "Enabled", value = true, type = TYPE_BOOL, attach = "Enabled",
			changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null]), actions = Dictionary ({slot = "_module_activation ()"})})
		}));
		"""@Description: Controls the verification of values ​​of the differents inputs of a module. This option is only available in edit mode."""
		self.bind_prop (Dictionary ({source = "AutoCompile", value = true, type = TYPE_BOOL, attach = "AutoCompile",
			changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null])})
		}));
		"""@Description: Enables module print log when it working. This feature will allow you to follow all process's debug allocate by the module in real time."""
		self.bind_prop (Dictionary ({source = "Verbose", value = false, type = TYPE_BOOL, attach = "Verbose",
			changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null])})
		}));
		"""@Description: Enumeration condition the execution environment of a module. This option restricts the scope of execution of a module."""
		self.bind_prop (Dictionary ({source = "ActivityZone", dropdown = self.ActivityArea.keys (), value = 2, attach = "ActivityZone", min = 0, max = 2,
			changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null]), actions = Dictionary ({slot = "_module_activation ()"})})
		}));
		"""
			@Description: Would you like the module in question supports the multiplayer system provided by this framework ? Note that for this works, it will
				defraud that you use the load module to manage a game configured in multiplayer mode.
		"""
		self.bind_prop (Dictionary ({source = "Multiplayer", value = false, type = TYPE_BOOL, attach = "Multiplayer",
			changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null])})
		}));
		"""
			@Description: Boolean which, once activated, gives an overview of the functioning of the module in question. In other words, this option accomplishes
				the task main of a module according to the configurations carried out at its level. This option is not present in all cases. It depends on the nature
				and operation of the module in question. The scope of this field is only on the Godot engine.
		"""
		self.bind_prop (Dictionary ({source = "Simulate", attach = "Simulate", button = Dictionary ({actions = Dictionary ({slot = "simulate ()"})})}));
		"""@Description: Controls the reset of values ​​of the different inputs of module. The scope of this field is only on the Godot engine."""
		self.bind_prop (Dictionary ({source = "ResetValues", attach = "ResetValues", button = Dictionary ({actions = Dictionary ({slot = "reset_values ()"})})}));

# Contains all basics saveable properties of a Godot Mega Assets module.
func _basics_saveable_module_properties () -> void:
	# Apply protected visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PROTECTED, PoolStringArray (["Saveable", "Recordable"])):
		# Contains the module title category.
		self.bind_prop (Dictionary ({title = "Saveable", index = 0}));
		"""
			@Description: Which data manager do you want choose you ? It is according to the latter that the data of the module in question will be loaded and
				or saved. This option is only available on saveable modules.
		"""
		self.bind_prop (Dictionary ({source = "DataManager", value = 0, type = TYPE_INT, dropdown = self.GameDataManager.keys (),
			attach = PoolStringArray (["DataManager", "Checkpoint", "Section", "GlobalKey", "SaveData", "LoadData"]),
			changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null])}), min = 0, max = 2, require = Array ([
				{statement = "DataManager == 0", actions = Array ([Dictionary ({slot = "SaveData", value = false}), Dictionary ({slot = "LoadData", value = false})])},
			])
		}));
		"""
			@Description: On which checkpoint, the data of the module will be saved and/or loaded ? Note that the precision of the latter is not mandatory. In this
				case, the active savepoint on the "SaveLoadFx" module will be taken for target. (Only use this field if the chosen data manager targets the module
				"SaveLoadFx"). This option is only available on saveable modules.
		"""
		self.bind_prop (Dictionary ({source = "Checkpoint", value = String (''), type = TYPE_STRING, attach = "Checkpoint", visible = false,
			changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null])}), showif = "DataManager == 1"
		}));
		"""
			@Description: On which configuration section, the data of the module will be saved and/or loaded ? Note that the accuracy of the latter is highly obligatory.
				(Only use this field if the chosen data manager targets the module "SettingsFx"). By default, a configuration section name is automatically generated
				by the module. You can change it to whatever you want. This option is not available only on saveable modules.
		"""
		self.bind_prop (Dictionary ({source = "Section", value = ((self.name + "Section") if !self.name.empty () else (self.get_class () + "Section")),
			type = TYPE_STRING, changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null])}), attach = "Section",
			require = Dictionary ({statement = "AutoCompile && not Section", actions = Dictionary ({message = "This property cannot be empty.",
			type = self.Message.ERROR})}), visible = false, showif = "DataManager == 2"
		}));
		"""
			@Description: Defines the global identifier to be used during data backups and uploads. By default, a key is generated by the module. You have the
				possibility of changing it to what you want. The role of this attribute is to allow updating data manager with data from multiple instances of
				the same module independently. Note that filling in this field is strongly required. This option is only available on saveable modules.
		"""
		self.bind_prop (Dictionary ({source = "GlobalKey", value = (self.name if !self.name.empty () else self.get_class ()), type = TYPE_STRING, visible = false,
			changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null])}), attach = "GlobalKey", showif = "DataManager > 0",
			require = Dictionary ({statement = "AutoCompile && !GlobalKey", actions = Dictionary ({message = "This property cannot be empty.", type = self.Message.ERROR})})
		}));
		"""
			@Description: Controls the saving of data within a module. In other words, do you want to automatically save module data all times game data manager
				will be saved ? This option is only available on saveable modules.
		"""
		self.bind_prop (Dictionary ({source = "SaveData", value = false, type = TYPE_BOOL, attach = "SaveData", showif = "DataManager > 0",
			changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null])}), visible = false, require = Dictionary ({
				statement = "AutoCompile && SaveData && DataManager == 0", actions = Dictionary ({slot = "SaveData", value = false,
					message = "This feature can't be enabled if no data manager has been specified.", type = self.Message.WARNING
				})
			})
		}));
		"""
			@Description: Controls the loading of data within a module. In other words, do you want to automatically load the data to the starting the game ?
				This option is only available on saveable modules.
		"""
		self.bind_prop (Dictionary ({source = "LoadData", value = false, type = TYPE_BOOL, attach = "LoadData", showif = "DataManager > 0",
			changed = Dictionary ({callback = "module_values_changed ()", params = Array ([null, null])}), visible = false, require = Dictionary ({
				statement = "AutoCompile && LoadData && DataManager == 0", actions = Dictionary ({slot = "LoadData", value = false,
					message = "This feature can't be enabled if no data manager has been specified.", type = self.Message.WARNING
				})
			})
		}));
		# Attaches "GlobalKey" and "Section" properties to "AutoCompile" property.
		self.override_prop (Dictionary ({attach = PoolStringArray (["AutoCompile", "GlobalKey", "Section"])}), "AutoCompile");

############################################################################## [Particulars variables] ######################################################################
# Contains all constants about module basics signals.
const _BASICS_SIGNALS = PoolStringArray (["enabled", "disabled", "start", "values_changed", "children_changed", "parent_changed"]);
# Contains all contants about naughty attributes manager.
enum NaughtyAttributes {
	INPUT_MAP = 0, SIGNALS = 1, TAGS = 2, METHODS = 3, TYPES = 4, OPERATORS = 5, MOUSE_CONTROLS = 6,
	GAMEPAD_CONTROLS = 7, DESKTOP_RESOLUTIONS = 8, IPAD_RESOLUTIONS = 9, IPHONE_RESOLUTIONS = 10,
	ANDROID_RESOLUTIONS = 11, KEYBOARD_CONTROLS = 12, SYSTEM_DIR = 13, GAME_CONTROLLERS = 14
};
# Contains all constants for game data manager.
enum GameDataManager {NONE = 0, GAME_SAVES = 1, GAME_CONFIGS = 2};
# Contains all constants about "bind_prop" function.
enum PropertyAccessMode {READ_ONLY = 0, WRITE_ONLY = 1, BOTH = 2};
# Contains all constants about module containers operations.
enum ContainerOperation {NONE = 0, SET = 1, ADD = 2, REMOVE = 3};
# Contains all constants about modules activity zone.
enum ActivityArea {EDITOR_ONLY = 0, RUNTIME_ONLY = 1, BOTH = 2};
# Contains all fixed "DataManager" mdule property listeners for this class.
var _data_manager_listeners: Array = Array (["DataManager", "Checkpoint", "Section", "GlobalKey", "SaveData", "LoadData"]) setget _unsetable_var_error;
# Contains all fixed "AutoCompile" mdule property listeners for this class.
var _auto_compile_listeners: Array = Array (["AutoCompile", "GlobalKey", "Section", "EventsBindings"]) setget _unsetable_var_error;
# Contains all found devices on computer.
var _devices_list: Array = Array ([]) setget _unsetable_var_error;
# Contains the old module children.
var _old_children: Array = Array ([]) setget _unsetable_var_error;
# Contains the module state.
var _is_initialized: bool = false setget _unsetable_var_error;
# Contains the old module child count.
var _old_child_count: int = 0 setget _unsetable_var_error;
# Contains the target game data manager reference.
var _data_manager = null setget _unsetable_var_error;
# Contains all properties data about child of this node.
var __props__: Dictionary = Dictionary ({});

############################################################################### [Properties manager] ########################################################################
# Unsetable module private variables.
func _unsetable_var_error (_new_value) -> void: self.output ("Can't change value of this variable. Because it private.", self.Message.ERROR, self);

############################################################################## [Logic and main tasks] #######################################################################
# This method is called on game initialization and before all nodes instanciation.
func _init () -> void: self._basics_module_properties ();

# Listens editor notifications.
func _notification (what: int) -> void: self._listen_notifications (what);

# Gets script variables list.
func _get_property_list () -> Array: return self.get_properties ();

# Called to return property value.
func _get (property: String): return self.get_prop (property);

# Called to set property editor value.
func _set (property: String, value): self.set_prop (property, value, true, 0.0);

# Manages destructible module initialization.
func _destructible_initialization (_wait: bool) -> void: if self.apply_visibility (self.MethodAccessMode.PRIVATE): pass;

# Manages indestructible module initialization.
func _indestructible_initialization (_wait: bool) -> void: if self.apply_visibility (self.MethodAccessMode.PRIVATE): pass;

# Throwns on first module idle frame.
func _awake () -> void: if self.apply_visibility (self.MethodAccessMode.PRIVATE): pass;

# Throwns on first module idle frame after awake method run.
func _start () -> void: if self.apply_visibility (self.MethodAccessMode.PRIVATE): pass;

# Called when the scene tree is ready.
func _ready () -> void: pass;

# Called before ready method run.
func _enter_tree () -> void:
	# For an indestructible module.
	if self._get_class_name () == "Indestructible" || self._get_class_name () == "Recordable":
		# When module is fully initialized, listens parent changements to warn handler.
		if !self._is_initialized and self.is_unlock ():
			# Throwns "parent_changed" event and makes a verbose.
			self._raise_module_event ("parent_changed"); self.verbose ("Parent changed !");
	# For a destructible module.
	elif self.is_game_initialised () and self.is_unlock ():
		# Throwns "parent_changed" event and makes a verbose.
		self._raise_module_event ("parent_changed"); self.verbose ("Parent changed !");
	# The module is it initialised ?
	if not self.is_initialized (): self._module_initialization (true); if Engine.editor_hint: self.verbose ("Parent changed !");

# This method is called when module is disabled/enabled.
func _module_activation () -> void:
	# Apply protected visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PROTECTED, "MegaAssets"):
		# Checks module activation.
		if self.is_unlock ():
			# Throwns "enabled" event and makes a verbose.
			self._raise_module_event ("enabled"); self.verbose ("Enabled !");
		else:
			# Throwns "disabled" event and makes a verbose.
			self._raise_module_event ("disabled"); self.verbose ("Disabled !");

# Checks "&&", "and" keyword expression.
func _check_and_keyword (cond) -> bool:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Contains all members seperated with "&&" or "and" statement keyword.
		cond = cond.replace ("and", "&&"); cond = cond.split ("&&"); for st in cond: if !self._check_single_condition (st): return false; return true;
	# Otherwise.
	else: return false;

# This method is called whether a gamepad is connected or disconnected.
func _on_game_controller_detected (_device: int, _connected: bool) -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Updates all dropdowns configurations as game controllers list.
		for drop in self._devices_list: self.override_prop (Dictionary ({"dropdown": Dictionary ({"behavior": NaughtyAttributes.GAME_CONTROLLERS})}), drop);

# Updates the target data manager with the module property(ies) value(s).
func _update_data_manager () -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Makes a verbose and throwns "before_update_data" event.
		self.verbose ("Updating game data manager..."); self._raise_module_event ("before_update_data");
		# Updates the game data manager and throwns "after_update_data" event.
		self._module_data_crud (0); self._raise_module_event ("after_update_data");

# Loads all module data from the target game data manager.
func _load_module_data () -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Makes a verbose and throwns "before_load_data" event.
		self.verbose ("Loading data..."); self._raise_module_event ("before_load_data");
		# Loads module data from the game data manager and throwns "after_load_data" event.
		self._module_data_crud (1); self._raise_module_event ("after_load_data");

# Saves the module data dynamically.
func _dynamic_save_data () -> void:
	# The module is it enabled ?
	if self.apply_visibility (self.MethodAccessMode.PROTECTED, PoolStringArray (["SaveLoadFx", "SettingsFx"])) and self.is_unlock ():
		# Contains the corrected form of the "SaveData" module property and checks whether we should save module data on any saving detection.
		var save_data = self.get_prop ("SaveData"); save_data = save_data if save_data is bool else false; if save_data: self._update_data_manager ();

# Loads the module data dynamically.
func _dynamic_load_data (_datum_count: int) -> void:
	# The module is it enabled ?
	if self.apply_visibility (self.MethodAccessMode.PROTECTED, PoolStringArray (["SaveLoadFx", "SettingsFx"])) and self.is_unlock ():
		# Contains the corrected form of the "LoadData" module property and checks whether we should load module data on any loading detection.
		var load_data = self.get_prop ("LoadData"); load_data = load_data if load_data is bool else false; if load_data: self._load_module_data ();

# This method is called on every game frames.
func _process (_delta: float) -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE) and self.is_unlock ():
		# Checking the current module child count at all times.
		if self.get_child_count () != self._old_child_count || hash (self.get_children ()) != hash (self._old_children):
			# Throwns "children_changed" event, updates the old module child count to the current child count.
			self._raise_module_event ("children_changed"); _old_child_count = self.get_child_count ();
			# Updates the old module children to the current children and makes a verbose.
			_old_children = self.get_children (); self.verbose ("Children changed !");

# Determinates whether data within of module have been saved into the game data manager.
func _is_saved () -> bool:
	# Is it a saveable module ?
	if self.is_saveable ():
		# Apply protected visibility to this method.
		if self.apply_visibility (self.MethodAccessMode.PROTECTED, PoolStringArray (["Saveable", "Recordable"])):
			# Checks whether all module data have been saved into game data manager and returns the final result.
			return (self._module_data_crud (2) if self.check_initialization () and self.is_unlock () else false);
	# Otherwise.
	else: self.output ("Can't call {is_saved} method because this module is not saveable.", self.Message.ERROR, self); return false;

# Updates the game data manager.
func _update_data (delay: float = 0.0) -> void:
	# Is it a saveable module ?
	if self.is_saveable ():
		# The module is it enabled ?
		if self.apply_visibility (self.MethodAccessMode.PROTECTED, PoolStringArray (["Saveable", "Recordable"])) && self.check_initialization () && self.is_unlock ():
			# Waiting for the given delay and updates the data manager with module property(ies) data.
			if delay > 0.0 and !Engine.editor_hint: yield (self.get_tree ().create_timer (delay), "timeout"); self._update_data_manager ();
	# Otherwise.
	else: self.output ("Can't call {update_data} method because this module is not saveable.", self.Message.ERROR, self);

# Back up data of module using the game's data management system.
func _save_data (delay: float = 0.0) -> void:
	# Is it a saveable module ?
	if self.is_saveable ():
		# The module is it enabled ?
		if self.apply_visibility (self.MethodAccessMode.PROTECTED, Array (["Saveable", "Recordable"])) && self.check_initialization () && self.is_unlock ()\
		&& self._check_data_manager (false, true) is bool:
			# Waiting for the given delay and saves the module data from the target data manager.
			if delay > 0.0 and !Engine.editor_hint: yield (self.get_tree ().create_timer (delay), "timeout"); self._save_module_data ();
	# Otherwise.
	else: self.output ("Can't call {save_data} method because this module is not saveable.", self.Message.ERROR, self);

# Loads data of module using the game's data management system.
func _load_data (delay: float = 0.0) -> void:
	# Is it a saveable module ?
	if self.is_saveable ():
		# The module is it enabled ?
		if self.apply_visibility (self.MethodAccessMode.PROTECTED, PoolStringArray (["Saveable", "Recordable"])) && self.check_initialization () && self.is_unlock ():
			# Waiting for the given delay and loads the module data from the target data manager.
			if delay > 0.0 and !Engine.editor_hint: yield (self.get_tree ().create_timer (delay), "timeout"); self._load_module_data ();
	# Otherwise.
	else: self.output ("Can't call {load_data} method because this module is not saveable.", self.Message.ERROR, self);

# This method destroys input event about gamepad detection manager.
func _destroy_input_event (pname: String) -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Removes this property whether it's not a dropdown devices.
		if self._devices_list.has (pname): _devices_list.erase (pname);
		# Is it connected ?
		var is_connected: bool = Input.is_connected ("joy_connection_changed", self, "_on_game_controller_detected");
		# No connection has been done.
		if self._devices_list.size () == 0 && is_connected: Input.disconnect ("joy_connection_changed", self, "_on_game_controller_detected");

# Throwns common basics module events.
func _thrown_basics_events (module_class_name: String, other_class_name: String = String (' ')) -> void:
	# Apply protected visibility on this method.
	if self.apply_visibility (self.MethodAccessMode.PROTECTED, PoolStringArray (["Destructible", "Indestructible"])):
		# The given module class is it equal to the current module class name ?
		if self._get_class_name () == module_class_name && other_class_name.length () || self._get_class_name () == other_class_name:
			# Getting module current name.
			var module_name: String = self.name if self.name.length () else self.get_class ();
			# Calls module entries methods and makes some verboses.
			_is_initialized = true; self._awake (); self.verbose (module_name + " initialized !"); if self.is_unlock ():
				# Calls start method and throwns "start" event.
				self._start (); self._raise_module_event ("start"); self.verbose (module_name + " started !");

# Returns a corresponding key of a few property name.
func _prop_name_accuracy (prop: String) -> String:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Checks whether the passed property name is already defined on the property(ies) manager.
		if not self.__props__.has (prop):
			# Searches the passed property into the property(ies) manager.
			for item in self.__props__: if item.to_lower ().ends_with (prop.to_lower ()): return item;
		# Returns the final result.
		return prop;
	# Otherwise.
	else: return "Null";

# Saves module data into the target game data manager.
func _save_module_data () -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Contains the corrected form of the "SaveData" module property.
		self.verbose ("Saving data..."); var save_data = self.get_prop ("SaveData"); save_data = save_data if save_data is bool else false;
		# Updates the given game data manager with the module property(ies) value(s) and throwns "before_save_data" event.
		if !save_data: self._update_data_manager (); self._raise_module_event ("before_save_data");
		# For game saves and configurations manager.
		if self.get_type (self._data_manager) == "SaveLoadFx": self._data_manager.save_game_data ();
		# Throwns "after_save_data" event.
		else: self._data_manager.save_game_configs (); self._raise_module_event ("after_save_data");

# Manages module origin initialization.
func _module_initialization (wait: bool) -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Checks the current module class name.
		if self._get_class_name () == "Module":
			# Adds basics module signals whether needed.
			for event in self._BASICS_SIGNALS:
				# Adds the current signal to this module reference.
				if !self.has_signal (event) or !self.has_user_signal (event): self.add_user_signal (event);
		# When the game is running.
		if !Engine.editor_hint:
			# Waiting for idle frame, enables module "AutoCompile" property and makes a verbose.
			if wait: yield (self.get_tree (), "idle_frame"); self.set_prop ("AutoCompile", true); self.verbose ("Module initializing...");
		# Getting the child count of the module and his children.
		_old_child_count = self.get_child_count (); _old_children = self.get_children (); self._thrown_basics_events ("Module");

# Runs first treatments for saveables properties of module.
func _saveable_initialization (wait: bool) -> void:
	# Apply protected visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PROTECTED, self._SCRIPTS_CORE):
		# Waiting for idle frame and corrects "LoadData" module property.
		if !Engine.editor_hint and wait: yield (self.get_tree (), "idle_frame"); var load_data = self.get_prop ("LoadData");
		# Checks module state and checks "LoadData" boolean value.
		load_data = load_data if load_data is bool else false; var data_manager = self.get_prop ("DataManager");
		# Contains the corrected form of the "DataManager" module property and checks whether a data manager module reference is defined on the current scene tree.
		data_manager = data_manager if data_manager is int else self.GameDataManager.NONE; self._fix_data_manager_events (data_manager > self.GameDataManager.NONE);
		# Contains the game data manager state.
		var data_manager_state: Array = self._check_data_manager_state (); if self.is_unlock () and load_data:
			# The game data manager module isn't initialized.
			if not data_manager_state [1]:
				# Waiting for "after_load_data" event call.
				if data_manager_state [0]: yield (self, "after_load_data");
			# The game data manager module is already initialized.
			elif data_manager_state [2] == "SaveLoadFx" || data_manager_state [2] == "SettingsFx": self._load_module_data ();
		# Makes a verbose and throwns basics module events.
		self.verbose ("Saveable initializing..."); self._thrown_basics_events ("Saveable", "Recordable");

# Checks the target data manager run constraints.
func _check_data_manager_state () -> Array:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Contains the corrected form of the "DataManager" module property.
		var data_manager = self.get_prop ("DataManager"); data_manager = data_manager if data_manager is int else self.GameDataManager.NONE;
		# Getting the current data manager reference type.
		var type: String = self.get_type (self._data_manager); if data_manager <= self.GameDataManager.NONE: return Array ([false, true, "Nil"]);
		# For any found data mnager.
		elif type == "SaveLoadFx" || type == "SettingsFx":
			# Contains the corrected form of the "TargetScenes" data manager property.
			var array = self._data_manager.get_prop ("TargetScenes"); array = Array (array) if array is PoolStringArray else Array ([]);
			# Contains the corrected form of the "LoadAllData" data manager property.
			var load_all_data = self._data_manager.get_prop ("LoadAllData"); load_all_data = load_all_data if load_all_data is bool else false;
			# The data manager isn't enabled.
			if !self._data_manager.is_unlock (): return Array ([false, self._data_manager.is_initialized (), type]);
			# No game data will be loaded.
			elif !load_all_data && !array.has (self.get_tree ().current_scene.name): return Array ([false, self._data_manager.is_initialized (), type]);
			# Otherwise.
			else: return Array ([true, self._data_manager.is_initialized (), type]);
		# For others cases.
		else: return Array ([false, true, "Nil"]);
	# Otherwise.
	else: return Array ([false, true, "Nil"]);

# Fix the target data manager events.
func _fix_data_manager_events (warnings: bool) -> void:
	# Fix the target game data manager events.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE) && !Engine.editor_hint && self._check_data_manager (false, warnings) is bool && self._data_manager != null:
		# Checks whether this module is already connected to "after_load" event of the target game data manager.
		if !self._data_manager.is_connected ("after_load", self, "_dynamic_load_data"):
			# Connects this module reference to "after_load" signal of the target game data manager.
			if self._data_manager.connect ("after_load", self, "_dynamic_load_data") != OK: pass;
		# Checks whether this module is already connected to "before_save" event of the target game data manager.
		if !self._data_manager.is_connected ("before_save", self, "_dynamic_save_data"):
			# Connects this module reference to "before_save" signal of the target game data manager.
			if self._data_manager.connect ("before_save", self, "_dynamic_save_data") != OK: pass;

# Checks "||", "or" keyword expression.
func _check_or_keyword (cond) -> bool:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Contains all members seperated with "||", "or" statement keyword.
		cond = cond.replace ("or", "||"); cond = cond.split ("||");
		# Checks statements.
		for st in cond:
			# "&&" or "and" statement keyword is detected.
			if st.find ("&&") != -1 || st.find ("and") != -1:
				# If ever a statement is true.
				if self._check_and_keyword (st): return true;
			# Otherwise.
			elif self._check_single_condition (st): return true;
	# Always return the truth.
	return false;

# Listens editor notifications to run configurations from script variables structure.
func _listen_notifications (what: int) -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Runs all notifications settings from the current property.
		for elmt in self.__props__:
			# For notification option.
			if self.__props__ [elmt].notification != null:
				# Dictionary type.
				if self.__props__ [elmt].notification is Dictionary: self._prop_require_notification_manager (elmt, self.__props__ [elmt].notification, what);
				# For an array type.
				elif self.__props__ [elmt].notification is Array:
					# Runs all configurations about each notification statement.
					for item in self.__props__ [elmt].notification: if item is Dictionary: self._prop_require_notification_manager (elmt, item, what);

# Manages clone searcher in dictionaries or arrays.
func _clone_manager (cfgs: Dictionary, prop: String) -> void:
	# Checks "actions" key existance.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE) and cfgs.has ("actions"):
		# Getting the passed statement.
		var statement = (cfgs.statement if cfgs.statement is String || cfgs.statement is bool else null) if cfgs.has ("statement") else null;
		# Ckecks the given statement expression and getting limit value from the given dictionary.
		statement = self._check_expression (statement) if statement is String else statement; var limit: int = cfgs.limit if cfgs.has ("limit") else 1;
		# A statemnt expression exists.
		if statement != null:
			# The id and statement keys exists.
			if statement && cfgs.has ("id") && self.get_clones_of (cfgs.id, self.__props__ [prop].value, limit) != null: self._actions_manager (cfgs.actions, prop);
		# Only id key exists.
		elif cfgs.has ("id") && self.get_clones_of (cfgs.id, self.__props__ [prop].value, limit) != null: self._actions_manager (cfgs.actions, prop);

# Manages generic module events throwing.
func _raise_module_event (event_name: String, params = self) -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Checks whether module has the target event.
		if self.has_signal (event_name) or self.has_user_signal (event_name):
			# Is it an indestructible module ?
			if self.is_dont_destroy_mode ():
				# Checks parameters type.
				if not params is Dictionary: self.emit_signal (event_name);
				# Otherwise.
				else:
					# Destroys "node" key from the given event data.
					if params.erase ("node"): pass; self.emit_signal (event_name, params);
			# Otherwise.
			else: self.emit_signal (event_name, params);
		# The given signal name is defined into the basics module signals.
		elif event_name in self._BASICS_SIGNALS:
			# Adds the current signal to module and calls it.
			self.add_user_signal (event_name); self._raise_module_event (event_name, params);
		# Otherwise.
		else: self.output (("Undefined {" + event_name + "} on this module."), self.Message.ERROR, self);

# Checks a statement expression.
func _check_expression (cond: String) -> bool:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Corrects the passed statement expression.
		cond = str_replace (cond, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' ');
		# Contains "&&", "and", "||", "or" keywords position.
		var res: Array = Array ([cond.begins_with ("||"), cond.ends_with ("||"), cond.begins_with ("or"), cond.ends_with ("or"),
			cond.begins_with ("&&"), cond.ends_with ("&&"), cond.begins_with ("and"), cond.ends_with ("and")
		]);
		# Don't begin and end with "||", "or", "&&", "and" keywords.
		if res [0] || res [1] || res [2] || res [3] || res [4] || res [5] || res [6] || res [7]:
			# Error message.
			self.output ("Invalid statement syntax.", self.Message.ERROR, self); return false;
		# Otherwise.
		else:
			# For "||", "or" statement logic.
			if cond.find ("||") != -1 || cond.find ("or") != -1: return self._check_or_keyword (cond);
			# For "&&", "and" statement logic.
			elif cond.find ("&&") != -1 || cond.find ("and") != -1: return self._check_and_keyword (cond); else: return self._check_single_condition (cond);
	# Otherwise.
	else: return false;

# Manages the given configurations about "require" and "notification" keys.
func _prop_require_notification_manager (prop: String, cfg: Dictionary, what: int) -> void:
	# Checks actions key presence.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE) and cfg.has ("actions"):
		# Searches what key.
		var wht: int = -1; if what >= 0: wht = cfg.what if cfg.has ("what") && cfg.what is int else -1;
		# The statement is not empty.
		if cfg.has ("statement") && cfg.statement is String:
			# Checks developper statement.
			if self._check_expression (cfg.statement):
				# For require and notification option.
				if what < 0 or what >= 0 && what == wht: self._actions_manager (cfg.actions, prop);
		# Otherwise.
		else:
			# For require option.
			if what < 0 && self.__props__ [prop].value == self.__props__ [prop].oldvalue: self._actions_manager (cfg.actions, prop);
			# For notification option.
			elif what >= 0 && what == wht: self._actions_manager (cfg.actions, prop);

# Is property value has changed ?
func _prop_value_changed (pname: String) -> void:
	# The changed key has a value.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE) and self.__props__ [pname].changed != null:
		# Calls a method with no parameters.
		if self.__props__ [pname].changed is String: self.invoke (self.__props__ [pname].changed, Array ([]), self);
		# Otherwise.
		elif self.__props__ [pname].changed is Dictionary:
			# Checks "callback" key into the given dictionary.
			if self.__props__ [pname].changed.has ("callback") && self.__props__ [pname].changed.callback is String:
				# The method has no parameters.
				if !self.__props__ [pname].changed.has ("params"): self.invoke (self.__props__ [pname].changed.callback, Array ([]), self);
				# Otherwise.
				else:
					# Corrects method parameters.
					self.__props__ [pname].changed.params = (Array (self.__props__ [pname].changed.params)\
						if self.is_array (self.__props__ [pname].changed.params) else Array ([self.__props__ [pname].changed.params]));
					# Duplicates method parameters data and searches and runs each special keyword.
					var params: Array = self.__props__ [pname].changed.params.duplicate (); for r in params.size (): params [r] = self._value_manager (params [r]);
					# Gets the current value of the current property.
					params [0] = pname if params.size () > 0 else params [0]; if params.size () > 1: params [1] = self.__props__ [pname].value;
					# Calls the given method with his name.
					self.invoke (self.__props__ [pname].changed.callback, params, self);
			# Checks actions key presence.
			if self.__props__ [pname].changed.has ("actions"): self._actions_manager (self.__props__ [pname].changed.actions, pname);

# Checks existance of "GlobalKey", "Section", "Checkpoint", "LoadData", "EventsBindings" and "SaveData" properties into "__props__" dictionary.
func _destroy_unusefull_props () -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# "AutoCompile" module property exists.
		if self.get_prop ("AutoCompile") is bool:
			# "EventsBindings" property has been destroyed.
			if not self.get_prop ("EventsBindings") is Array: _auto_compile_listeners.erase ("EventsBindings");
			# "GlobalKey" property has been destroyed.
			if not self.get_prop ("GlobalKey") is String: _auto_compile_listeners.erase ("GlobalKey");
			# "Section" property has been destroyed.
			if not self.get_prop ("Section") is String: _auto_compile_listeners.erase ("Section");
			# Overrides "attach" key of "AutoCompile" module property.
			self.override_prop (Dictionary ({attach = self._auto_compile_listeners}), "AutoCompile");
		# "DataManager" module property exists.
		if self.get_prop ("DataManager") is int:
			# "GlobalKey" property has been destroyed.
			if not self.get_prop ("GlobalKey") is String: _data_manager_listeners.erase ("GlobalKey");
			# "Section" property has been destroyed.
			if not self.get_prop ("Section") is String: _data_manager_listeners.erase ("Section");
			# "Checkpoint" property has been destroyed.
			if not self.get_prop ("Checkpoint") is String: _data_manager_listeners.erase ("Checkpoint");
			# "LoadData" property has been destroyed.
			if not self.get_prop ("LoadData") is bool: _data_manager_listeners.erase ("LoadData");
			# "SaveData" property has been destroyed.
			if not self.get_prop ("SaveData") is bool: _data_manager_listeners.erase ("SaveData");
			# Overrides "attach" key of "DataManager" module property.
			self.override_prop (Dictionary ({attach = self._data_manager_listeners}), "DataManager");

# Checks the given data manager and returns his reference or informations.
func _check_data_manager (other_components: bool, warnings: bool):
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Contains the selected data manager from the module.
		var data_manager = self.get_prop ("DataManager"); data_manager = data_manager if data_manager is int else self.GameDataManager.NONE;
		# Checks the passed game data manager.
		var type: String = self.get_type (self._data_manager); if data_manager > self.GameDataManager.NONE:
			# Game saves manager.
			if data_manager == self.GameDataManager.GAME_SAVES: _data_manager = self.get_data_manager (self) if type != "SaveLoadFx" else self._data_manager;
			# Game settings manager.
			else: _data_manager = self.get_settings_manager (self) if type != "SettingsFx" else self._data_manager;
			# Updating the game data manager type reference.
			type = self.get_type (self._data_manager) if type == "Nil" else type;
			# Checks whether a game data manager has been found.
			if type == "SaveLoadFx" || type == "SettingsFx":
				# Should us return additionals informations ?
				if other_components:
					# Gets data manager checkpoint/section property value.
					var container: String = str (self.get_prop ("Checkpoint")) if data_manager == self.GameDataManager.GAME_SAVES else str (self.get_prop ("Section"));
					# Corrects the module checkpoint/section value and converts it into string format for other treatments.
					container = container.lstrip (' ').rstrip (' ') if container is String else String ('');
					# Returns all informations about the target game data manager.
					return Array ([container, (str (self.get_prop ("GlobalKey")).replace (' ', '') + "$-$"), type]);
				# Otherwise.
				else: return true;
			# No data manager found.
			else: if warnings: self.output ("The target game data manager isn't defined.", self.Message.WARNING, self);
		# Warning message.
		else: if warnings: self.output ("No data manager has been specified.", self.Message.WARNING, self);
	# Returns a nullable value for others cases.
	return null;

# Destroys all data linked to a data manager module. Warning ! there will be no going back after the destruction of the latter.
func _remove_data (delay: float = 0.0) -> void:
	# Is it a saveable module ?
	if self.is_saveable ():
		# The module is it enabled ?
		if self.apply_visibility (self.MethodAccessMode.PROTECTED, PoolStringArray (["Saveable", "Recordable"])) and self.is_unlock ():
			# Contains the target game data manager.
			var manager_data = self._check_data_manager (true, true);
			# A data manager has been found.
			if manager_data is Array:
				# Waiting for the given delay and then throwns "before_destroy_data" event.
				if delay > 0.0 and !Engine.editor_hint: yield (self.get_tree ().create_timer (delay), "timeout");
				# Makes a verbose and throwns "before_destroy_data" event.
				self.verbose ("Destroying data..."); self._raise_module_event ("before_destroy_data");
				# A container has been specified.
				if not manager_data [0].empty ():
					# For game saves manager.
					if manager_data [2] == "SaveLoadFx": self._data_manager.destroy_checkpoints (manager_data [0]);
					# For configurations manager.
					else: self._data_manager.destroy_sections (manager_data [0]);
				# Destroys all associated module key(s) from the target container.
				else:
					# Destroys all saveable property(ies) from the target data manager.
					for prop in self.__props__:
						# Is it a saveable property ?
						if self.__props__ [prop].saveable:
							# Getting the property id.
							var property_id: String = (manager_data [1] + (prop + ".prop").get_file ().split ('.') [0]);
							# The current data manager is an instance of SaveLoadFx module.
							if manager_data [2] == "SaveLoadFx": self._data_manager.destroy_keys (property_id);
							# The current data manager is an instance of SettingsFx module.
							else: self._data_manager.destroy_keys (manager_data [0], property_id);
				# Throwns "after_destroy_data" event.
				self._raise_module_event ("after_destroy_data");
	# Otherwise.
	else: self.output ("Can't call {remove_data} method because this module is not saveable.", self.Message.ERROR, self);

# Returns all members with her data from the given statement expression.
func _get_members (cond: String, opt: String) -> Array:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Don't begin and end with the given operator.
		if cond.begins_with (opt) || cond.ends_with (opt): self.output ("Invalid statement syntax.", self.Message.ERROR, self);
		# Otherwise.
		else:
			# Contains all members from the given statement.
			var mem: Array = Array (cond.split (opt));
			# Checks array size.
			if mem.size () == 2:
				# Removes left and right spaces.
				mem [0] = mem [0].lstrip (' ').rstrip (' '); mem [1] = mem [1].lstrip (' ').rstrip (' ');
				# All statement members aren't empty.
				if !mem [0].empty () && !mem [1].empty ():
					# Checks left statement.
					mem.append (mem [0].begins_with ('!') or mem [0].begins_with ("not"));
					mem [0] = mem [0].split ('!') [1].lstrip (' ') if mem [0].begins_with ('!') else mem [0];
					mem [0] = mem [0].split ("not") [1].lstrip (' ') if mem [0].begins_with ("not") else mem [0]; mem [0] = self._get_member (mem, 0);
					# Is it '?' defined ?
					if not mem [1].begins_with ('?'):
						# The operator is not "is" keyword.
						if opt != "is": mem [1] = Array ([self.get_variant (mem [1]), true]); else: mem [1] = Array ([mem [1].to_lower (), true]);
					# Otherwise.
					else:
						# Removes '?' character.
						mem [1] = mem [1].split ('?') [1].lstrip (' ');
						# The operator is not "is" keyword.
						if opt != "is": mem [1] = self._get_member (mem, 1); else: mem [1] = Array ([mem [1].to_lower (), true]);
					# Returns the final result.
					return mem;
				# Error message.
				else: self.output ("Invalid statement syntax.", self.Message.ERROR, self);
			# Error message.
			else: self.output ("Syntax error.", self.Message.ERROR, self);
	# Returns an empty array.
	return Array ([]);

# Runs "UPDATE", "LOAD" and "STATE" actions from the module property(ies).
func _module_data_crud (action: int):
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Contains the target game data manager informations.
		var manager_data = self._check_data_manager (true, true);
		# No warnings detected.
		if manager_data is Array:
			# Removes the last element of the given array.
			manager_data.pop_back (); var has_keys_container = null if manager_data [0].empty () else manager_data [0];
			# Updates the game data manager with the given module property(ies) value(s).
			for prop in self.__props__:
				# Is it a saveable property ?
				if self.__props__ [prop].saveable:
					# Generates the real property id.
					var prop_id: String = (manager_data [1] + (prop + ".prop").get_file ().split ('.') [0]);
					# For "UPDATE" action.
					if action <= 0: self._data_manager.set_data (prop_id, self.__props__ [prop].value, manager_data [0]);
					# For "LOAD" action.
					elif action == 1:
						# The game is running.
						if !Engine.editor_hint: self.__props__ [prop].value = self._data_manager.get_data (prop_id, self.__props__ [prop].value, manager_data [0]);
						# Otherwise.
						else: self.override_prop (Dictionary ({value = self._data_manager.get_data (prop_id, self.__props__ [prop].value, manager_data [0])}), prop);
					# For "STATE" action.
					else:
						# The current property id is it defined on the game data manager ?
						if self._data_manager.has_keys (prop_id, has_keys_container):
							# Gets the current property value.
							var value = self._data_manager.get_data (prop_id, null, manager_data [0]);
							# Checks the loaded property value.
							if typeof (value) == typeof (self.__props__ [prop].value) and hash (value) == hash (self.__props__ [prop].value): pass; else: return false;
						# Otherwise.
						else: return false;
			return true;
		# Otherwise.
		else: return false;

# Gets value from a property or method name.
func _value_manager (value):
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# For string format.
		if typeof (value) == TYPE_STRING:
			# Removes left and right spaces.
			value = value.lstrip (' ').rstrip (' ');
			# No errors detected.
			if value.count ('?') <= 1 && value.count ("()") <= 1:
				# The given string is a name of a method or property.
				if value.begins_with ('?'):
					# Removes '?' character letter.
					value = value.split ('?') [1].lstrip (' ');
					# Is it a method ?
					if not value.ends_with ("()"):
						# Corrects property name.
						var val: String = self._prop_name_accuracy (value);
						# The property is it defined ? and check whether the given property is defined on the script variables.
						if self.__props__.has (val): return self.__props__ [val].value; elif not self.is_undefined (value, self): return self.get (value);
						# Error message.
						else: self.output (("Undefined {" + value + "} property."), self.Message.ERROR, self);
					# Otherwise.
					else: return self.invoke (value.lstrip ('(').lstrip (')').split ("()") [0], Array ([]), self);
				# Assigns the given value with no changement.
				else: return value;
			# Error message.
			else: self.output ("Invalid syntax.", self.Message.ERROR, self);
		# For dictionary format.
		elif typeof (value) == TYPE_DICTIONARY && value.has ("callback") && value.callback is String:
			# Checks "parameters" key.
			if value.has ("params"):
				# Getting method parameters.
				value.params = Array ([value.params]) if !self.is_array (value.params) else Array (value.params);
				# Duplicates method parameters data.
				var parms: Array = value.params.duplicate (); for k in parms.size (): parms [k] = self._value_manager (parms [k]);
				# Returns method result.
				return self.invoke (value.callback, parms, self);
			# Otherwise.
			else: return self.invoke (value.callback, Array ([]), self);
		# Assigns the given value with no changement.
		else: return value;
	# Returns a nullable value.
	else: return null;

# Manages all given actions from keys configurations.
func _actions_manager (actions, prop: String) -> void:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Getting object prefix and converting configurations data into an data array.
		var prefix: String = self.get_object_prefix (self); actions = Array ([actions]) if not actions is Array else actions;
		# Runs all configurations.
		for cfg in actions:
			# The current configuration is a dictionary.
			if cfg is Dictionary:
				# Searches message key.
				if cfg.has ("message") && cfg.message is String:
					# Searches type key.
					if cfg.has ("type") && cfg.type is int: self._output ((prefix + " -> " + prop + ": " + cfg.message), cfg.type);
					# Error message configurations.
					else: self._output ((prefix + " -> " + prop + ": " + cfg.message), self.Message.ERROR);
				# Checks "slot" key on the given dictionary.
				if cfg.has ("slot") && cfg.slot is String:
					# Corrects the given slot name.
					cfg.slot = str_replace (cfg.slot, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' ');
					# Is it a method ?
					if not cfg.slot.ends_with ("()"):
						# Searches the real property name.
						cfg.slot = self._prop_name_accuracy (cfg.slot);
						# The value key is defined.
						if cfg.has ("value"):
							# Checks property existance.
							if self.__props__.has (cfg.slot):
								# The property value isn't a dictionary.
								if not cfg.value is Dictionary && typeof (cfg.value) == typeof (self.__props__ [cfg.slot].value):
									# The current value isn't equal to his old value.
									if cfg.value != self.__props__ [cfg.slot].value: self.override_prop (Dictionary ({value = cfg.value}), cfg.slot);
								# Overrides the target property with the given configurations.
								elif cfg.value is Dictionary: self.override_prop (cfg.value.duplicate (), cfg.slot);
							# The given property is defiened on the script variables.
							elif not self.is_undefined (cfg.slot, self): self.set (cfg.slot, cfg.value);
							# Otherwise.
							else: self.output (("Undefined {" + cfg.slot + "} property."), self.Message.ERROR, self);
					# Otherwise.
					else:
						# The method has no parameters.
						if !cfg.has ("params"): self.invoke (cfg.slot.lstrip ('(').lstrip (')').split ("()") [0], Array ([]), self);
						# Otherwise.
						else:
							# Corrects method parameters.
							cfg.params = Array (cfg.params) if self.is_array (cfg.params) else Array ([cfg.params]);
							# Duplicates method parameters data and then searches and runs each special keyword.
							var parms: Array = cfg.params.duplicate (); for m in parms.size (): parms [m] = self._value_manager (parms [m]);
							# Calls the given method with his name.
							self.invoke (cfg.slot.lstrip ('(').lstrip (')').split ("()") [0], parms, self);

# Returns the member statement value with additional data.
func _get_member (mem: Array, idx: int):
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Checks '.' and "()" characters count.
		if mem [idx].count ('.') <= 1 && mem [idx].count ("()") <= 1:
			# Checks '.' and "()" characters position.
			if !mem [idx].begins_with ('.') and !mem [idx].ends_with ('.') and !mem [idx].begins_with ("()"):
				# Determines wether the final result will dynamic or not.
				var sim: bool = true; if mem [idx].find ('.') == -1:
					# The key is not a method.
					if not mem [idx].ends_with ("()"):
						# Corrects property name and checks script properties data.
						mem [idx] = self._prop_name_accuracy (mem [idx]); if self.__props__.has (mem [idx]): mem [idx] = self.__props__ [mem [idx]].value;
						# Physic script variable.
						elif not self.is_undefined (mem [idx], self): mem [idx] = self.get (mem [idx]);
						# Error message.
						else: self.output (("Undefined {" + mem [idx] + "} property."), self.Message.ERROR, self);
					# Otherwise.
					else: mem [idx] = self.invoke (mem [idx].split ("()") [0], Array ([]), self);
				# Otherwise.
				else:
					# Gets filtered value.
					mem [idx] = Array (mem [idx].split ('.')); mem [idx] [0].lstrip (' ').rstrip (' '); mem [idx] [1].lstrip (' ').rstrip (' ');
					# The second member is an integer.
					mem [idx] [1] = int (mem [idx] [1]) if self.is_full_numbers (mem [idx] [1]) else mem [idx] [1];
					# The key is not a method.
					if not mem [idx] [0].ends_with ("()"):
						# Corrects property name and checks script properties data.
						mem [idx] [0] = self._prop_name_accuracy (mem [idx] [0]); if self.__props__.has (mem [idx] [0]):
							# Is it dynamic ?
							sim = true if self.is_array (self.__props__ [mem [idx] [0]].value) && mem [idx] [1] is int else false;
							# Updates members array.
							mem [idx] = self.get_id_value_of (mem [idx] [1], self.__props__ [mem [idx] [0]].value);
						# Physic script variable.
						elif not self.is_undefined (mem [idx] [0], self):
							# Is it dynamic ?
							sim = true if self.is_array (self.get (mem [idx] [0])) && mem [idx] [1] is int else false;
							# Updates members array.
							mem [idx] = self.get_id_value_of (mem [idx] [1], self.get (mem [idx] [0]));
						# Error message.
						else: self.output (("Undefined {" + mem [idx] [0] + "} property."), self.Message.ERROR, self);
					# Otherwise.
					else:
						# Gets method result value.
						var mdh = self.invoke (mem [idx] [0].split ("()") [0], Array ([]), self); sim = true if self.is_array (mdh) && mem [idx] [1] is int else false;
						# Gets the final result value.
						mem [idx] = self.get_id_value_of (mem [idx] [1], mdh);
				# Getting the final result.
				return Array ([mem [idx], sim]);
			# Error message.
			else: self.output ("Invalid statement syntax.", self.Message.ERROR, self);
		# Error message.
		else: self.output ("Syntax error.", self.Message.ERROR, self);
	# Returns a nullable value for others cases.
	return null;

# Manages properties range value on integers, reals, vector2 and vector3.
func _prop_value_range (val, limit, less: bool, idx: int = -1):
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# For a single integer and real.
		if typeof (val) == TYPE_INT || typeof (val) == TYPE_REAL:
			# Returns the given range.
			if less: return max (limit, val); else: return min (limit, val);
		# For a Vector2.
		elif typeof (val) == TYPE_VECTOR2:
			# For min option.
			if less:
				# The min value is a vector2 or vector3.
				if limit is Vector2 || limit is Vector3:
					# Checks index value.
					if idx < 0 || idx > 1: return Vector2 (max (limit.x, val.x), max (limit.y, val.y)); elif idx == 0: return Vector2 (max (limit.x, val.x), val.y);
					# Otherwise.
					else: return Vector2 (val.x, max (limit.y, val.y));
				# The min value is an integer or real.
				else:
					# Checks index value.
					if idx < 0 || idx > 1: return Vector2 (max (limit, val.x), max (limit, val.y)); elif idx == 0: return Vector2 (max (limit, val.x), val.y);
					# Otherwise.
					else: return Vector2 (val.x, max (limit, val.y));
			# For max option.
			else:
				# The max value is a vector2 or vector3.
				if limit is Vector2 || limit is Vector3:
					# Checks index value.
					if idx < 0 || idx > 1: return Vector2 (min (limit.x, val.x), min (limit.y, val.y)); elif idx == 0: return Vector2 (min (limit.x, val.x), val.y);
					# Otherwise.
					else: return Vector2 (val.x, min (limit.y, val.y));
				# The max value is an integer or real.
				else:
					# Checks index value.
					if idx < 0 || idx > 1: return Vector2 (min (limit, val.x), min (limit, val.y)); elif idx == 0: return Vector2 (min (limit, val.x), val.y);
					# Otherwise.
					else: return Vector2 (val.x, min (limit, val.y));
		# For a vector3.
		elif typeof (val) == TYPE_VECTOR3:
			# For min option.
			if less:
				# The min value is a vector2.
				if limit is Vector2:
					# Checks index value.
					if idx < 0 || idx > 1: return Vector3 (max (limit.x, val.x), max (limit.y, val.y), val.z);
					elif idx == 0: return Vector3 (max (limit.x, val.x), val.y, val.z); else: return Vector3 (val.x, max (limit.y, val.y), val.z);
				# The min value is a vector3.
				elif limit is Vector3:
					# Checks index value.
					if idx < 0 || idx > 4: return Vector3 (max (limit.x, val.x), max (limit.y, val.y), max (limit.z, val.z));
					elif idx == 0: return Vector3 (max (limit.x, val.x), val.y, val.z); elif idx == 1: return Vector3 (val.x, max (limit.y, val.y), val.z);
					elif idx == 2: return Vector3 (val.x, val.y, max (limit.z, val.z)); elif idx == 3: return Vector3 (max (limit.x, val.x), val.y, max (limit.z, val.z));
					else: return Vector3 (val.x, max (limit.y, val.y), max (limit.z, val.z));
				# The min value is an integer or real.
				else:
					# Checks index value.
					if idx < 0 || idx > 4: return Vector3 (max (limit, val.x), max (limit, val.y), max (limit, val.z));
					elif idx == 0: return Vector3 (max (limit, val.x), val.y, val.z); elif idx == 1: return Vector3 (val.x, max (limit, val.y), val.z);
					elif idx == 2: return Vector3 (val.x, val.y, max (limit, val.z)); elif idx == 3: return Vector3 (max (limit, val.x), val.y, max (limit, val.z));
					else: return Vector3 (val.x, max (limit, val.y), max (limit, val.z));
			# For max option.
			else:
				# The max value is a vector2.
				if limit is Vector2:
					# Checks index value.
					if idx < 0 || idx > 1: return Vector3 (min (limit.x, val.x), min (limit.y, val.y), val.z);
					elif idx == 0: return Vector3 (min (limit.x, val.x), val.y, val.z); else: return Vector3 (val.x, min (limit.y, val.y), val.z);
				# The max value is a vector3.
				elif limit is Vector3:
					# Checks index value.
					if idx < 0 || idx > 4: return Vector3 (min (limit.x, val.x), min (limit.y, val.y), min (limit.z, val.z));
					elif idx == 0: return Vector3 (min (limit.x, val.x), val.y, val.z); elif idx == 1: return Vector3 (val.x, min (limit.y, val.y), val.z);
					elif idx == 2: return Vector3 (val.x, val.y, min (limit.z, val.z)); elif idx == 3: return Vector3 (min (limit.x, val.x), val.y, min (limit.z, val.z));
					else: return Vector3 (val.x, min (limit.y, val.y), min (limit.z, val.z));
				# The max value is an integer or real.
				else:
					# Checks index value.
					if idx < 0 || idx > 4: return Vector3 (min (limit, val.x), min (limit, val.y), min (limit, val.z));
					elif idx == 0: return Vector3 (min (limit, val.x), val.y, val.z); elif idx == 1: return Vector3 (val.x, min (limit, val.y), val.z);
					elif idx == 2: return Vector3 (val.x, val.y, min (limit, val.z)); elif idx == 3: return Vector3 (max (limit, val.x), val.y, min (limit, val.z));
					else: return Vector3 (val.x, min (limit, val.y), min (limit, val.z));
		# Otherwise.
		else: return val;

# Corrects a certains entries from the given property script variables.
func _correct_data (data: Dictionary) -> Array:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Checks all keys.
		var checks: Array = Array ([data.has ("source"), data.has ("value"), data.has ("type"), data.has ("hint"), data.has ("usage"), data.has ("visible"),
			data.has ("stream"), data.has ("private"), data.has ("changed"), data.has ("index"), data.has ("duplicate"), data.has ("button"), data.has ("enabled"),
			data.has ("disableif"), data.has ("showif"), data.has ("require"), data.has ("min"), data.has ("max"), data.has ("clone"), data.has ("notification"),
			data.has ("attach"), data.has ("saveable")
		]);
		# Destroys input event and checks value key.
		if checks [0]: self._destroy_input_event (data.source); if checks [1]: data.value = self._value_manager (data.value);
		# Checks attach key.
		if checks [20]: data.attach = PoolStringArray (Array ([data.attach]) if not self.is_array (data.attach) else Array (data.attach));
		# Checks title key.
		if data.has ("title") && data.title is String:
			# Configures script title.
			data.source = data.title; data.usage = PROPERTY_USAGE_CATEGORY; data.type = TYPE_NIL; data.value = null;
			checks [0] = true; checks [4] = true; checks [2] = true; checks [1] = true;
		# Checks dropdown key.
		if data.has ("dropdown"):
			# Configures dropdown.
			data.type = TYPE_INT; data.hint = PROPERTY_HINT_ENUM; var is_dic: bool = data.dropdown is Dictionary; checks [2] = true; checks [3] = true;
			# Checks dropdown behavior.
			if is_dic && data.dropdown.has ("behavior") && data.dropdown.behavior is int:
				# For InputMap behavior.
				if data.dropdown.behavior <= self.NaughtyAttributes.INPUT_MAP: data._hint_str = InputMap.get_actions ();
				# For Signals behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.SIGNALS:
					# Contains all true availables signals names on the given object.
					var filter: Array = Array ([]);
					# Filtering empty strings.
					for s in self.get_id_value_of ("name", self.get_signal_list ()):
						# Removes left and right spaces.
						s = s.lstrip (' ').rstrip (' '); if !s.empty () and !filter.has (s): filter.append (s);
					# Updates dropdown to object signals list.
					data._hint_str = filter;
				# For Tags behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.TAGS: data._hint_str = self.get_groups ();
				# For Methods behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.METHODS:
					# Contains all true availables methods names on the given object.
					var filter: Array = Array ([]);
					# Filtering empty strings.
					for m in self.get_id_value_of ("name", self.get_method_list ()):
						# Removes left and right spaces and check some contraints.
						m = m.lstrip (' ').rstrip (' '); if !m.empty () and !m.begins_with ("arg") and not filter.has (m): filter.append (m);
					# Updates dropdown to object methods list.
					data._hint_str = filter;
				# For data types behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.TYPES: data._hint_str = self.get_godot_base_types ();
				# For operators behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.OPERATORS: data._hint_str = self.get_godot_operators ();
				# For Mouse controls behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.MOUSE_CONTROLS:
					# Updates dropdown to mouse controls.
					data._hint_str = self.get_mouse_controls ();
					# Checks keycodes key.
					if data.dropdown.has ("keycodes") && data.dropdown.keycodes is bool && data.dropdown.keycodes: data._drop_opt = self.NaughtyAttributes.MOUSE_CONTROLS;
				# For GamePad controls behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.GAMEPAD_CONTROLS:
					# Updates dropdown to gamepad controls.
					data._hint_str = self.get_joystick_controls ();
					# Checks keycodes key.
					if data.dropdown.has ("keycodes") && data.dropdown.keycodes is bool && data.dropdown.keycodes: data._drop_opt = self.NaughtyAttributes.GAMEPAD_CONTROLS;
				# For desktop resolutions behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.DESKTOP_RESOLUTIONS:
					# Updates dropdown to desktop resolutions.
					data._hint_str = self.get_desktop_resolutions ();
					# Checks sizes key.
					if data.dropdown.has ("sizes") && data.dropdown.sizes is bool && data.dropdown.sizes: data._drop_opt = self.NaughtyAttributes.DESKTOP_RESOLUTIONS;
				# For ipad resolutions behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.IPAD_RESOLUTIONS:
					# Updates dropdown to ipad resolutions.
					data._hint_str = self.get_ipad_resolutions ();
					# Checks sizes key.
					if data.dropdown.has ("sizes") && data.dropdown.sizes is bool && data.dropdown.sizes: data._drop_opt = self.NaughtyAttributes.IPAD_RESOLUTIONS;
				# For iphone resolutions behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.IPHONE_RESOLUTIONS:
					# Updates dropdown to iphone resolutions.
					data._hint_str = self.get_iphone_resolutions ();
					# Checks sizes key.
					if data.dropdown.has ("sizes") && data.dropdown.sizes is bool && data.dropdown.sizes: data._drop_opt = self.NaughtyAttributes.IPHONE_RESOLUTIONS;
				# For android resolutions behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.ANDROID_RESOLUTIONS:
					# Updates dropdown to android resolutions.
					data._hint_str = self.get_android_resolutions ();
					# Checks sizes key.
					if data.dropdown.has ("sizes") && data.dropdown.sizes is bool && data.dropdown.sizes: data._drop_opt = self.NaughtyAttributes.ANDROID_RESOLUTIONS;
				# For keyboard controls behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.KEYBOARD_CONTROLS:
					# Updates dropdown to keyboard controls.
					data._hint_str = self.get_keyboard_controls ();
					# Checks keycodes key.
					if data.dropdown.has ("keycodes") && data.dropdown.keycodes is bool && data.dropdown.keycodes: data._drop_opt = self.NaughtyAttributes.KEYBOARD_CONTROLS;
				# For system directories behavior.
				elif data.dropdown.behavior == self.NaughtyAttributes.SYSTEM_DIR:
					# Updates dropdown to system directories.
					data._hint_str = Path.keys ();
					# Checks paths key.
					if data.dropdown.has ("paths") && data.dropdown.paths is bool && data.dropdown.paths: data._drop_opt = self.NaughtyAttributes.SYSTEM_DIR;
				# For game controllers behavior.
				else:
					# No connection has been done.
					if !Input.is_connected ("joy_connection_changed", self, "_on_game_controller_detected"):
						# Makes a connection to game controllers inputs.
						if Input.connect ("joy_connection_changed", self, "_on_game_controller_detected") != OK:
							# Error message.
							self.output ("Connection error on {joy_connection_changed} event.", Message.ERROR, self);
					# The dropdowns devices cash has no source.
					if not self._devices_list.has (data.source): _devices_list.insert (0, data.source);
					# Updates dropdown to game controllers inputs list.
					data._hint_str = self.get_connected_controllers_names ();
			# Otherwise.
			else:
				# Corrects dropdown input.
				data._hint_str = data.dropdown if !is_dic else ''; data._hint_str = data.dropdown.value if is_dic && data.dropdown.has ("value") else data._hint_str;
				# Converts dropdown into an array.
				data._hint_str = Array ([data._hint_str]) if !self.is_array (data._hint_str) else Array (data._hint_str);
			# Converts dropdown into a string.
			data._hint_str = PoolStringArray (data._hint_str).join (',');
		# Checks button key.
		elif checks [11]:
			# Configures button.
			data.type = TYPE_BOOL; data.value = false; checks [1] = true; checks [2] = true;
		# Checks range key.
		elif data.has ("range") && checks [1] && checks [2]:
			# Checks variable valur type.
			if data.type == TYPE_INT || data.type == TYPE_REAL:
				# The given range value type is a vector.
				var pams: String = (str (data.range.x) + ',' + str (data.range.y)) if data.range is Vector2 || data.range is Vector3 else '';
				# Parameters is a vector.
				if not pams.empty ():
					# Configures range.
					data.hint = PROPERTY_HINT_RANGE; pams = (pams + ',' + str (data.range.z)) if data.range is Vector3 else pams; data._hint_str = pams; checks [3] = true;
		# Checks hint string key and returns the final result.
		elif data.has ("hint_string") && data.hint_string is String: data._hint_str = data.hint_string; return Array ([data, checks]);
	# Returns an empty array.
	else: return Array ([]);

# Checks a single statement expression.
func _check_single_condition (cond: String) -> bool:
	# Apply private visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PRIVATE):
		# Corrects the passed statement expression.
		cond = cond.lstrip (' ').rstrip (' '); if cond.empty (): self.output ("No statement found.", self.Message.ERROR, self);
		# Otherwise.
		else:
			# Contains the result of all searches.
			var res: PoolIntArray = PoolIntArray ([cond.find ("<="), cond.find ('>'), cond.find (">="), cond.find ('<'), cond.count ('='), cond.find ("!=")]);
			# For "<=" and '>' operators.
			if res [0] != -1 || res [1] != -1 && res [4] == 0:
				# Gets all members data from the given condition.
				var mems: Array = self._get_members (cond, '>') if res [1] != -1 else self._get_members (cond, "<=");
				# The result is detected.
				if !mems.empty () and mems [0] is Array and mems [1] is Array:
					# Supports his opposite operator.
					if res [0] != -1 && !mems [2]: mems [2] = true; elif res [0] != -1 && mems [2]: mems [2] = false;
					# For a single operator.
					if mems [0] [1] and mems [1] [1]:
						if !mems [2]: return typeof (mems [0] [0]) == typeof (mems [1] [0]) && mems [0] [0] > mems [1] [0];
						else: return typeof (mems [0] [0]) == typeof (mems [1] [0]) && not mems [0] [0] > mems [1] [0];
					# For a complex operation.
					elif !mems [0] [1] and mems [1] [1]:
						mems [0] [0] = Array ([mems [0] [0]]) if not mems [0] [0] is Array else mems [0] [0];
						for item in mems [0] [0]:
							if !mems [2] && typeof (item) == typeof (mems [1] [0]) && item > mems [1] [0]: return true;
							elif mems [2] && typeof (item) == typeof (mems [1] [0]) && not item > mems [1] [0]: return true;
					# For a complex operation.
					elif mems [0] [1] and !mems [1] [1]:
						mems [1] [0] = Array ([mems [1] [0]]) if not mems [1] [0] is Array else mems [1] [0];
						for item in mems [1] [0]:
							if !mems [2] && typeof (mems [0] [0]) == typeof (item) && mems [0] [0] > item: return true;
							elif mems [2] && typeof (mems [0] [0]) == typeof (item) && not mems [0] [0] > item: return true;
					# For more complex operation.
					else:
						mems [0] [0] = Array ([mems [0] [0]]) if not mems [0] [0] is Array else mems [0] [0];
						mems [1] [0] = Array ([mems [1] [0]]) if not mems [1] [0] is Array else mems [1] [0];
						for item in mems [0] [0]:
							for elmt in mems [1] [0]:
								if !mems [2] && typeof (item) == typeof (elmt) && item > elmt: return true;
								elif mems [2] && typeof (item) == typeof (elmt) && not item > elmt: return true;
			# For ">=" and '<' operators.
			elif res [2] != -1 || res [3] != -1 && res [4] == 0:
				# Gets all members data from the given condition.
				var mems: Array = self._get_members (cond, '<') if res [3] != -1 else self._get_members (cond, ">=");
				# The result is detected.
				if !mems.empty () and mems [0] is Array and mems [1] is Array:
					# Supports his opposite operator.
					if res [2] != -1 && !mems [2]: mems [2] = true; elif res [2] != -1 && mems [2]: mems [2] = false;
					# For a single operator.
					if mems [0] [1] and mems [1] [1]:
						if !mems [2]: return typeof (mems [0] [0]) == typeof (mems [1] [0]) && mems [0] [0] < mems [1] [0];
						else: return typeof (mems [0] [0]) == typeof (mems [1] [0]) && not mems [0] [0] < mems [1] [0];
					# For a complex operation.
					elif !mems [0] [1] and mems [1] [1]:
						mems [0] [0] = Array ([mems [0] [0]]) if not mems [0] [0] is Array else mems [0] [0];
						for item in mems [0] [0]:
							if !mems [2] && typeof (item) == typeof (mems [1] [0]) && item < mems [1] [0]: return true;
							elif mems [2] && typeof (item) == typeof (mems [1] [0]) && not item < mems [1] [0]: return true;
					# For a complex operation.
					elif mems [0] [1] and !mems [1] [1]:
						mems [1] [0] = Array ([mems [1] [0]]) if not mems [1] [0] is Array else mems [1] [0];
						for item in mems [1] [0]:
							if !mems [2] && typeof (mems [0] [0]) == typeof (item) && mems [0] [0] < item: return true;
							elif mems [2] && typeof (mems [0] [0]) == typeof (item) && not mems [0] [0] < item: return true;
					# For more complex operation.
					else:
						mems [0] [0] = Array ([mems [0] [0]]) if not mems [0] [0] is Array else mems [0] [0];
						mems [1] [0] = Array ([mems [1] [0]]) if not mems [1] [0] is Array else mems [1] [0];
						for item in mems [0] [0]:
							for elmt in mems [1] [0]:
								if !mems [2] && typeof (item) == typeof (elmt) && item < elmt: return true;
								elif mems [2] && typeof (item) == typeof (elmt) && not item < elmt: return true;
			# For '=' and "!=" operators.
			elif cond.find ('=') != -1:
				# Gets all members data from the given condition.
				var mems: Array = Array ([]);
				# Filtering keywords.
				if res [5] != -1: mems = self._get_members (cond, "!="); elif res [4] == 2: mems = self._get_members (cond, "==");
				elif res [4] == 1: mems = self._get_members (cond, '=');
				# The result is detected.
				if !mems.empty () and mems [0] is Array and mems [1] is Array:
					# Supports his opposite operator.
					if res [5] != -1 && !mems [2]: mems [2] = true; elif res [5] != -1 && mems [2]: mems [2] = false;
					# For a single operator.
					if mems [0] [1] and mems [1] [1]:
						if mems [0] [0] is Dictionary && mems [1] [0] is Dictionary:
							if !mems [2]: return mems [0] [0].hash () == mems [1] [0].hash (); else: return not mems [0] [0].hash () == mems [1] [0].hash ();
						else:
							if !mems [2]: return typeof (mems [0] [0]) == typeof (mems [1] [0]) && mems [0] [0] == mems [1] [0]\
								|| typeof (mems [0] [0]) == TYPE_STRING && mems [0] [0].empty () && mems [1] [0].to_lower () == "null";
							else: return typeof (mems [0] [0]) == typeof (mems [1] [0]) && not mems [0] [0] == mems [1] [0]\
								|| typeof (mems [0] [0]) == TYPE_STRING && !mems [0] [0].empty () && mems [1] [0].to_lower () == "null";
					# For a complex operation.
					elif !mems [0] [1] and mems [1] [1]:
						mems [0] [0] = Array ([mems [0] [0]]) if not mems [0] [0] is Array else mems [0] [0];
						for item in mems [0] [0]:
							if item is Dictionary && mems [1] [0] is Dictionary:
								if !mems [2]: return item.hash () == mems [1] [0].hash (); else: return not item.hash () == mems [1] [0].hash ();
							else:
								if !mems [2] && typeof (item) == typeof (mems [1] [0]) && item == mems [1] [0]: return true;
								elif !mems [2] && typeof (item) == TYPE_STRING && item.empty () && mems [1] [0].to_lower () == "null": return true;
								elif mems [2] && typeof (item) == typeof (mems [1] [0]) && not item == mems [1] [0]: return true;
								elif mems [2] && typeof (item) == TYPE_STRING && !item.empty () && mems [1] [0].to_lower () == "null": return true;
					# For a complex operation.
					elif mems [0] [1] and !mems [1] [1]:
						mems [1] [0] = Array ([mems [1] [0]]) if not mems [1] [0] is Array else mems [1] [0];
						for item in mems [1] [0]:
							if item is Dictionary && mems [1] [0] is Dictionary:
								if !mems [2]: return item.hash () == mems [0] [0].hash (); else: return not item.hash () == mems [0] [0].hash ();
							else:
								if !mems [2] && typeof (mems [0] [0]) == typeof (item) && mems [0] [0] == item: return true;
								elif mems [2] && typeof (mems [0] [0]) == typeof (item) && not mems [0] [0] == item: return true;
					# For more complex operation.
					else:
						mems [0] [0] = Array ([mems [0] [0]]) if not mems [0] [0] is Array else mems [0] [0];
						mems [1] [0] = Array ([mems [1] [0]]) if not mems [1] [0] is Array else mems [1] [0];
						for item in mems [0] [0]:
							for elmt in mems [1] [0]:
								if item is Dictionary && elmt is Dictionary:
									if !mems [2]: return item.hash () == elmt.hash (); else: return not item.hash () == elmt.hash ();
								else:
									if !mems [2] && typeof (item) == typeof (elmt) && item == elmt: return true;
									elif mems [2] && typeof (item) == typeof (elmt) && not item == elmt: return true;
			# For "is" operator.
			elif cond.find ("is") != -1:
				# Gets all members data from the given condition.
				var mems: Array = self._get_members (cond, "is");
				# The result is detected.
				if !mems.empty () and mems [0] is Array and mems [1] is Array:
					# For a single operator.
					if mems [0] [1]:
						if mems [0] [0] is NodePath:
							if mems [1] [0] == "nodepath": return true if !mems [2] else false; elif mems [1] [0] == "nil": return false if !mems [2] else true;
							mems [0] [0] = self.get_node_or_null (mems [0] [0]); if mems [0] [0] == null: return false;
						if !mems [2]: return self.get_type (mems [0] [0]).to_lower () == mems [1] [0];
						else: return not self.get_type (mems [0] [0]).to_lower () == mems [1] [0];
					# For a complex operation.
					else:
						mems [0] [0] = Array ([mems [0] [0]]) if not mems [0] [0] is Array else mems [0] [0];
						for item in mems [0] [0]:
							if item is NodePath:
								if item == "nodepath": return true if !mems [2] else false; elif item == "nil": return false if !mems [2] else true;
								item = self.get_node_or_null (item); if item == null: return false;
							if !mems [2] && self.get_type (item).to_lower () == mems [1] [0]: return true;
							elif mems [2] && not self.get_type (item).to_lower () == mems [1] [0]: return true;
			# For other statement.
			else:
				# Corrects statement and get all members.
				cond = (cond + "=false"); var mems: Array = self._get_members (cond, '='); mems [1] = null;
				# The result is detected.
				if !mems.empty () and mems [0] is Array:
					# For a single operator.
					if mems [0] [1]:
						if !mems [2] and mems [0] [0]: return true; elif mems [2] and !mems [0] [0]: return true;
					# For a complex operation.
					else:
						mems [0] [0] = Array ([mems [0] [0]]) if not mems [0] [0] is Array else mems [0] [0];
						for item in mems [0] [0]:
							if !mems [2] && item: return true; elif mems [2] && !item: return true;
	# Returns a fake value.
	return false;

# Runs module property constraints configurations.
func _run_constraints (prop: String) -> void:
	# The given property is ti defined ?
	if self.apply_visibility (self.MethodAccessMode.PRIVATE) and self.__props__.has (prop):
		# For disableif option.
		if self.__props__ [prop].disableif != null:
			# Boolean type.
			if self.__props__ [prop].disableif is bool: self.__props__ [prop].enabled = !self.__props__ [prop].disableif;
			# String type.
			elif self.__props__ [prop].disableif is String: self.__props__ [prop].enabled = !self._check_expression (self.__props__ [prop].disableif);
			# Dictionary type.
			elif self.__props__ [prop].disableif is Dictionary && self.__props__ [prop].disableif.has ("statement"):
				# For boolean type.
				if self.__props__ [prop].disableif.statement is bool: self.__props__ [prop].enabled = !self.__props__ [prop] [prop].disableif.statement;
				# For string type.
				elif self.__props__ [prop].disableif.statement is String:
					# Checks developper statement expression.
					self.__props__ [prop].enabled = !self._check_expression (self.__props__ [prop].disableif.statement);
					# Whether the given condition is true.
					if !self.__props__ [prop].enabled && self.__props__ [prop].disableif.has ("actions"):
						# Runs the given actions.
						self._actions_manager (self.__props__ [prop].disableif.actions, prop);
		# For showif option.
		if self.__props__ [prop].showif != null:
			# Contains the old visibility value.
			var old_visible: bool = self.__props__ [prop].visible;
			# Boolean type.
			if self.__props__ [prop].showif is bool: self.__props__ [prop].visible = self.__props__ [prop].showif;
			# String type.
			elif self.__props__ [prop].showif is String: self.__props__ [prop].visible = self._check_expression (self.__props__ [prop].showif);
			# Dictionary type.
			elif self.__props__ [prop].showif is Dictionary && self.__props__ [prop].showif.has ("statement"):
				# For boolean type.
				if self.__props__ [prop].showif.statement is bool: self.__props__ [prop].visible = self.__props__ [prop].showif.statement;
				# For string type.
				elif self.__props__ [prop].showif.statement is String:
					# Checks developper statement expression.
					self.__props__ [prop].visible = self._check_expression (self.__props__ [prop].showif.statement);
					# Whether the given condition is true.
					if self.__props__ [prop].visible && self.__props__ [prop].showif.has ("actions"): self._actions_manager (self.__props__ [prop].showif.actions, prop);
			# Refreshes module property.
			if old_visible != self.__props__ [prop].visible: self.property_list_changed_notify ();
		# For require option.
		if self.__props__ [prop].require != null:
			# String type.
			if self.__props__ [prop].require is String:
				# The property value is equal to his old value.
				if self.__props__ [prop].value == self.__props__ [prop].oldvalue:
					# Shows the passed message.
					self._output ((self.get_object_prefix (self) + " -> " + prop +  ": " + self.__props__ [prop].require), self.Message.ERROR);
			# Dictionary type.
			elif self.__props__ [prop].require is Dictionary: self._prop_require_notification_manager (prop, self.__props__ [prop].require, -1);
			# For an array type.
			elif self.__props__ [prop].require is Array:
				# Runs all configurations about each require statement.
				for elmt in self.__props__ [prop].require: if elmt is Dictionary: self._prop_require_notification_manager (prop, elmt, -1);
		# For min option.
		if self.__props__ [prop].min != null:
			# Contains some useful data.
			var res: Array = Array ([self.__props__ [prop].value is int, self.__props__ [prop].value is float, self.__props__ [prop].min is Vector2,
				self.__props__ [prop].min is Vector3, self.__props__ [prop].min is int, self.__props__ [prop].min is float
			]);
			# Float, integer, vector2 and vector3 type.
			if res [2] || res [3] || res [4] || res [5]:
				# The property is an integer or float.
				if res [0] || res [1]:
					# The min value is a vector2 or vector3.
					if res [2] || res [3]: self.__props__ [prop].min = self.__props__ [prop].min.x;
				# Converts min value into integer.
				if res [0]: self.__props__ [prop].min = int (self.__props__ [prop].min);
				# Updates the current value to the min value.
				self.__props__ [prop].value = self._prop_value_range (self.__props__ [prop].value, self.__props__ [prop].min, true, -1);
			# Dictionary type.
			elif self.__props__ [prop].min is Dictionary and self.__props__ [prop].min.has ("value"):
				# Adds some data.
				res.append (self.__props__ [prop].min.value is int); res.append (self.__props__ [prop].min.value is float);
				res.append (self.__props__ [prop].min.value is Vector2); res.append (self.__props__ [prop].min.value is Vector3);
				res.append (self.__props__ [prop].min.has ("index"));
				# The given value is a number or vector.
				if res [6] || res [7] || res [8] || res [9]:
					# The property is an integer or float.
					if res [0] || res [1]:
						# The min value is a vector2 or vector3.
						if res [8] || res [9]: self.__props__ [prop].min.value = self.__props__ [prop].min.value.x;
					# Converts min value into integer.
					if res [0]: self.__props__ [prop].min.value = int (self.__props__ [prop].min.value);
					# Getting index value.
					var idx: int = self.__props__ [prop].min.index if res [10] && self.__props__ [prop].min.index is int else -1;
					# Updates the current value to the min value.
					self.__props__ [prop].value = self._prop_value_range (self.__props__ [prop].value, self.__props__ [prop].min.value, true, idx);
					# Checks "actions" key presence.
					if self.__props__ [prop].min.has ("actions"):
						# Only on integer and real.
						if res [0] && res [6] || res [1] && res [7] || res [0] && res [7] || res [1] && res [6]:
							# Whether the current value is set to his min value.
							if self.__props__ [prop].value == self.__props__ [prop].min.value: self._actions_manager (self.__props__ [prop].min.actions, prop);
		# For max option.
		if self.__props__ [prop].max != null:
			# Contains some useful data.
			var res: Array = Array ([self.__props__ [prop].value is int, self.__props__ [prop].value is float, self.__props__ [prop].max is Vector2,
				self.__props__ [prop].max is Vector3, self.__props__ [prop].max is int, self.__props__ [prop].max is float
			]);
			# Float, integer, vector2 and vector3 type.
			if res [2] || res [3] || res [4] || res [5]:
				# The property is an integer or float.
				if res [0] || res [1]:
					# The max value is a vector2 or vector3.
					if res [2] || res [3]: self.__props__ [prop].max = self.__props__ [prop].max.x;
				# Converts max value into integer.
				if res [0]: self.__props__ [prop].max = int (self.__props__ [prop].max);
				# Updates the current value to the max value.
				self.__props__ [prop].value = self._prop_value_range (self.__props__ [prop].value, self.__props__ [prop].max, false, -1);
			# Dictionary type.
			elif self.__props__ [prop].max is Dictionary and self.__props__ [prop].max.has ("value"):
				# Adds some data.
				res.append (self.__props__ [prop].max.value is int); res.append (self.__props__ [prop].max.value is float);
				res.append (self.__props__ [prop].max.value is Vector2); res.append (self.__props__ [prop].max.value is Vector3);
				res.append (self.__props__ [prop].max.has ("index"));
				# The given value is a number or vector.
				if res [6] || res [7] || res [8] || res [9]:
					# The property is an integer or float.
					if res [0] || res [1]:
						# The max value is a vector2 or vector3.
						if res [8] || res [9]: self.__props__ [prop].max.value = self.__props__ [prop].max.value.x;
					# Converts max value into integer.
					if res [0]: self.__props__ [prop].max.value = int (self.__props__ [prop].max.value);
					# Getting index value.
					var idx: int = self.__props__ [prop].max.index if res [10] && self.__props__ [prop].max.index is int else -1;
					# Updates the current value to the max value with an id.
					self.__props__ [prop].value = self._prop_value_range (self.__props__ [prop].value, self.__props__ [prop].max.value, false, idx);
					# Checks "actions" key presence.
					if self.__props__ [prop].max.has ("actions"):
						# Only on integer and real.
						if res [0] && res [6] || res [1] && res [7] || res [0] && res [7] || res [1] && res [6]:
							# Whether the current value is set to his max value.
							if self.__props__ [prop].value == self.__props__ [prop].max.value: self._actions_manager (self.__props__ [prop].max.actions, prop);
		# Only on dictionary and array.
		if self.__props__ [prop].type == TYPE_ARRAY && typeof (self.__props__ [prop].value) == TYPE_ARRAY\
		|| self.__props__ [prop].type == TYPE_DICTIONARY && typeof (self.__props__ [prop].value) == TYPE_DICTIONARY:
			# For clone option.
			if self.__props__ [prop].clone != null:
				# Parsing clone configurations.
				var clone_configs = Array ([self.__props__ [prop].clone]) if !is_array (self.__props__ [prop].clone) else Array (self.__props__ [prop].clone);
				# Runs all clones request.
				for elmt in clone_configs: if elmt is Dictionary: self._clone_manager (elmt, prop);
		# For duplicate option.
		if self.__props__ [prop].type == TYPE_ARRAY && typeof (self.__props__ [prop].value) == TYPE_ARRAY && self.__props__ [prop].duplicate:
			# Searches nil element.
			for pos in self.__props__ [prop].value.size ():
				# Getting value of the preview element from the array.
				if typeof (self.__props__ [prop].value [pos]) == TYPE_NIL: self.__props__ [prop].value [pos] = self.__props__ [prop].value [(pos - 1)];
		# For button option.
		if self.__props__ [prop].button != null && self.__props__ [prop].value:
			# For string format.
			if self.__props__ [prop].button is String: self.invoke (self.__props__ [prop].button, Array ([]), self);
			# For a dictionary format.
			elif self.__props__ [prop].button is Dictionary && self.__props__ [prop].button.has ("actions"):
				# Runs the given actions whether there are defined.
				self._actions_manager (self.__props__ [prop].button.actions, prop);
			# Enabled button.
			self.__props__ [prop].value = false;
	# Error message.
	else: self.output (("Undefined {" + prop + "} property."), self.Message.ERROR, self);

# Returns a base class name.
func _get_class_name () -> String: return "Module" if self.apply_visibility (self.MethodAccessMode.PRIVATE) else "Null";

############################################################################### [Availables features] #######################################################################
"""@Description: Returns module version."""
static func get_version () -> String: return "0.2.6";

"""@Description: Returns module origins."""
static func get_origin_name () -> String: return "MegaAssets.Module";

"""@Description: Determinates whether module is saveable."""
static func is_saveable () -> bool: return false;

"""@Description: Is the module is indestructible ?"""
static func is_dont_destroy_mode () -> bool: return false;

"""@Description: Returns module class name."""
func get_class () -> String: return "Module";

"""@Description: The current module instance is it already initialized ?"""
func is_initialized () -> bool: return self._is_initialized;

"""
	@Description: Resets values ​​of the differents inputs of module.
	@Parameters:
		float delay: What is the timeout before reseting module properties ?
"""
func reset_values (delay: float = 0.0) -> void: self.reset_props_value ("ResetValues", null, delay);

"""
	@Description: Returns the current data of all properties about a module.
	@Parameters:
		bool json: Do you want to get data as json format ?
"""
func get_properties_data (json: bool = false):
	# Returns the current data from script properties.
	if json: return JSON.print (self.__props__, "\t"); else: return self.__props__;

"""
	@Description: Performs the three operations (MODIFY, ADD and DELETE) only on arrays and dictionaries.
	@Parameters:
		String name: What is your container name ?
		Variant id: Which container identifier do you want reach you ? Note that if the container is a list, the identifier must be an integer.
		Variant value: What value attributed to the identifier of the targeted container ? Note that the value assignment is not typed.
		int operation: Which is the operation to be performed on the targeted container ? The possible values ​​are:
			-> MegaAssets.ContainerOperation.NONE or 0: Nothing would happen.
			-> MegaAssets.ContainerOperation.SET or 1: Modify the value of an identifier in a container.
			-> MegaAssets.ContainerOperation.ADD or 2: Add a new value to a container. If the container is an array and its identifier is not defined or invalid,
				the element will be appended to the end of the latter. In the case of a dictionary, the targeted key is created when it does not exist
				in the dictionary or update otherwise.
			-> MegaAssets.ContainerOperation.REMOVE or 3: Remove an identifier from a container. If the given identifier is outside the limits of the container
				(table or dictionary), we will see a cleaning full of the latter.
		float delay: What is the idle time before updating the targeted container ?
"""
func set_container (name: String, id, value, operation: int = self.ContainerOperation.SET, delay: float = 0.0) -> void:
	# Changes value of the given container.
	self.set_containers (Dictionary ({"name": name, "id": id, "value": value, "operation": operation, timeout = 0.0}), delay);

"""
	@Description: Opens the documentation associated with this class.
	@Parameters:
		Node object: Which node will be considered to perform the different operations ?
		String feature: The documentation will target which functionality of style ?
		float delay: What is the deadtime before the opening of the documentation ?
"""
static func open_doc (object: Node, feature: String = String (''), delay: float = 0.0) -> void:
	# Opens the documentation.
	open_doc_manager (object, "https://godot-mega-assets.herokuapp.com/docs/bases/module", feature, delay);

"""
	@Description: Gives an overview of operation of module. In other words, it performs the main task of a module by depending on the configurations
		carried out at its level. This function is not present in all cases. It depends on the nature and operation of the module in question.
	@Parameters:
		float delay: What is the timeout before module simulation ?
"""
func simulate (delay: float = 0.0) -> void:
	# On editor only.
	if Engine.editor_hint:
		# Waiting for the given delay and shows a message after this death time.
		if delay > 0.0: yield (self.get_tree ().create_timer (delay), "timeout"); self.verbose ("Start simulation...");
	# On game running.
	else: self.output ("Can't make a module simulation when the game is running.", self.Message.WARNING, self);

"""
	@Description: This method is used when any module property value has been changed.
	@Parameters:
		String name: Contains the property name which value has been changed.
		Variant value: Contains the new property value.
		float delay: What is the timeout before call "values_changed" event on the module ?
"""
func module_values_changed (name: String, value, delay: float = 0.0) -> void:
	# Apply protected visibility to this method.
	if apply_visibility (self.MethodAccessMode.PROTECTED, (self._SCRIPTS_CORE + PoolStringArray ([self.get_class ()]))):
		# Enabled module "AutoCompile" property wether the game is running and checks module state.
		if !Engine.editor_hint: self.set_prop ("AutoCompile", true); if self.is_unlock ():
			# Waiting for the given delay.
			if delay > 0.0 and !Engine.editor_hint: yield (self.get_tree ().create_timer (delay), "timeout");
			# Throwns "values_changed" event.
			if !self.is_dont_destroy_mode (): self.emit_signal ("values_changed", Dictionary ({"name": name, "value": value, "node": self}));
			# For indestructible modules.
			else: self.emit_signal ("values_changed", Dictionary ({"name": name, "value": value})); self.verbose ("Values changed !");

"""@Description: Manages module activity zone and activation to determinates whether module is enabled or not."""
func is_unlock () -> bool:
	# Contains the corrected form of the "Enabled" module property and checks module activation.
	var enabled = self.get_prop ("Enabled"); enabled = enabled if enabled is bool else false; if enabled:
		# Contains the corrected form of the "ActivityZone" module property.
		var activity_zone = self.get_prop ("ActivityZone"); activity_zone = activity_zone if activity_zone is int else self.ActivityArea.BOTH;
		# For editor mode.
		if Engine.editor_hint and activity_zone == self.ActivityArea.EDITOR_ONLY: return true;
		# For runtime mode.
		elif !Engine.editor_hint and activity_zone == self.ActivityArea.RUNTIME_ONLY: return true;
		# For editor and runtime modes.
		elif activity_zone == self.ActivityArea.BOTH: return true; else: return false;
	# Otherwise.
	else: return false;

"""
	@Description: Restarts module. Made very careful during module reboots. This can be problematic in certain cases.
	@Parameters:
		float delay: What is the timeout before restarting module ?
"""
func restart (delay: float = 0.0) -> void:
	# The module is it enabled ?
	if self.check_initialization () and self.is_unlock ():
		# Waiting for the given delay and resets all module particulars properties.
		if delay > 0.0 and !Engine.editor_hint: yield (self.get_tree ().create_timer (delay), "timeout"); _old_child_count = 0; _old_children.clear ();
		# A data manager has been already specified.
		if self._data_manager != null:
			# This module reference is it connected to "after_load" signal of the game data manager ?
			if self._data_manager.is_connected ("after_load", self, "_dynamic_load_data"):
				# Disconnects this module reference from "after_load" signal of the game data manager.
				if self._data_manager.disconnect ("after_load", self, "_dynamic_load_data") != OK: pass;
			# This module reference is it connected to "before_save" signal of the game data manager ?
			if self._data_manager.is_connected ("before_save", self, "_dynamic_save_data"):
				# Disconnects this module reference from "before_save" signal of the game data manager.
				if self._data_manager.disconnect ("before_save", self, "_dynamic_save_data") != OK: pass;
		# Initializes some values before global restarting.
		_is_initialized = false; _data_manager = null; self.verbose ("Module restarting..."); self._module_initialization (false);
		# For destructible module class definition.
		if self._get_class_name () == "Destructible" or self._get_class_name () == "Saveable": self._destructible_initialization (false);
		# For indestructible module class definition.
		elif self._get_class_name () == "Indestructible" or self._get_class_name () == "Recordable": self._indestructible_initialization (false);
		# For module itself class definition.
		else:
			# Calls the last definition of Godot "_enter_tree ()" and "_ready ()" methods.
			self._enter_tree (); self._ready ();

"""
	@Description: Resets value of one or many properties.
	@Parameters:
		String trigger: Which property name calls this method ? It's very important whether you want to reset value of all script properties.
		Variant prop: Contains all properties names which value will be reset.
		float delay: What is the timeout before starting reset ?
"""
func reset_props_value (trigger: String, prop = null, delay: float = 0.0) -> void:
	# Game is running.
	if !Engine.editor_hint && delay > 0.0: yield (self.get_tree ().create_timer (delay), "timeout");
	# Gets all module properties.
	prop = self.__props__.keys () if prop == null && len (self.__props__) > 0 else prop;
	# Property isn't null.
	if prop != null:
		# Corrects the given trigger name.
		trigger = self.str_replace (trigger, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' ');
		# Converts prop into an PoolStringArray.
		prop = PoolStringArray (Array ([prop]) if !self.is_array (prop) else Array (prop));
		# Searches each all given properties.
		for p in prop:
			# Removes left and right spaces.
			p = p.lstrip (' ').rstrip (' '); p = self._prop_name_accuracy (p);
			# The current property is defined.
			if self.__props__.has (p):
				# Gets the old property value.
				self.__props__ [p].value = self.__props__ [p]._oldvalue;
				# The current property name is not equal to the trigger.
				if !trigger.empty () and p != trigger:
					# Calls the given callback whether it exists and runs property constraints.
					self._prop_value_changed (p); self._run_constraints (p);
		# Refreshes the given property value.
		self.property_list_changed_notify ();

"""
	@Description: Performs the three operations (MODIFY, ADD and DELETE) only on arrays and dictionaries.
	@Parameters:
		Dictionary | Array configs: Contains the different configurations on how each container in the module is managed. The dictionary (s) of
			this method supports the following keys:
			-> String name: What is your container name ?
			-> Variant id: Which container identifier do you want reach you ? Note that if the container is a list, the identifier must be an integer.
			-> Variant value: What value attributed to the identifier of the targeted container ? Note that the value assignment is not typed.
			-> float timeout = 0.0: Should us wait for the given delay before executing the given configurations ?
			-> int operation = MegaAssets.ContainerOperation.SET: Which is the operation to be performed on the targeted container ? The possible values ​​are:
				-> MegaAssets.ContainerOperation.NONE or 0: Nothing would happen.
				-> MegaAssets.ContainerOperation.SET or 1: Modify the value of an identifier in a container.
				-> MegaAssets.ContainerOperation.ADD or 2: Add a new value to a container. If the container is an array and its identifier is not defined or invalid,
					the element will be appended to the end of the latter. In the case of a dictionary, the targeted key is created when it does not exist
					in the dictionary or update otherwise.
				-> MegaAssets.ContainerOperation.REMOVE or 3: Remove an identifier from a container. If the given identifier is outside the limits of the container
					(table or dictionary), we will see a cleaning full of the latter.
		float delay: What is the idle time before updating the targeted container ?
"""
func set_containers (configs, delay: float = 0.0) -> void:
	# Waiting for the given delay.
	if delay > 0.0 and !Engine.editor_hint: yield (self.get_tree ().create_timer (delay), "timeout");
	# Converting the passed configurations into an array.
	configs = Array ([configs]) if not configs is Array else Array (configs);
	# Runs all availables configurations.
	for cfg in configs:
		# Is it a dictionary and has a valid condition ?
		if cfg is Dictionary and cfg.has ("name") and cfg.name is String and not cfg.name.empty ():
			# Contains the current module property value.
			var prop_value = self.get_prop (cfg.name); var is_array: bool = self.is_array (prop_value);
			# Getting the current id value.
			cfg.id = cfg.id if cfg.has ("id") else null; cfg.operation = int (cfg.operation) if cfg.has ("operation") && is_number (cfg.operation) else 1;
			# Checks id key existance.
			if prop_value != null and cfg.operation > self.ContainerOperation.NONE:
				# For an array.
				if is_array && self.is_number (cfg.id):
					# Disables "duplicate" key effect on any container.
					self.override_prop (Dictionary ({duplicate = false}), cfg.name);
					# "Set" operation.
					if cfg.operation == self.ContainerOperation.SET && self.index_validation (cfg.id, prop_value.size ()) && cfg.has ("value"):
						# The current value is a dictionary.
						if cfg.value is Dictionary && prop_value [int (cfg.id)] is Dictionary: for elmt in cfg.value: prop_value [int (cfg.id)] [elmt] = cfg.value [elmt];
						# Otherwise.
						else: prop_value [int (cfg.id)] = cfg.value;
					# "Add" operation.
					elif cfg.operation == self.ContainerOperation.ADD && cfg.has ("value"):
						# Adding the given value to the target container.
						if self.is_range (int (cfg.id), 0, (len (prop_value) - 1)): prop_value.insert (int (cfg.id), cfg.value); else: prop_value.append (cfg.value);
					# "Remove" operation.
					else:
						# Removing the given value to the target container.
						if self.is_range (int (cfg.id), 0, (len (prop_value) - 1)): prop_value.remove (int (cfg.id)); else: prop_value.clear ();
				# For a dictionary.
				elif prop_value is Dictionary:
					# For "Add" and "Set" operation.
					if cfg.operation == self.ContainerOperation.ADD || cfg.operation == self.ContainerOperation.SET: prop_value [cfg.id] = cfg.value;
					# For "Remove" operation.
					elif prop_value.has (cfg.id) && prop_value.erase (cfg.id): pass; else: prop_value.clear ();
				# Updates the target module property.
				self.set_prop (cfg.name, prop_value, false, (float (cfg.timeout) if cfg.has ("timeout") && self.is_number (cfg.timeout) else 0.0));
				# Re-checks the module property value.
				if is_array: self.override_prop (Dictionary ({duplicate = Engine.editor_hint}), cfg.name);

"""
	@Description: Changes value of the given property. This one must be defined on "__props__" property.
	@Parameters:
		String pname: Contains the property name.
		Variant value: Contains the property value.
		bool wait: Do you want to wait for the event "_enter_tree" is called before the initialization and execution of different configurations
			made in the properties of the script ? Do not activate this parameter only in the following methods:
			"_init", "_set", "_get", "_get_property_list", "_notification" or "_get_configuration_warning".
		float delay: What is the timeout before changing property value ?
"""
func set_prop (pname: String, value, wait: bool = false, delay: float = 0.0) -> void:
	# Corrects the given property name.
	pname = str_replace (pname, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' ');
	# If the property name is not empty.
	if not pname.empty ():
		# Waiting for user delay and getting exactly key value.
		if !Engine.editor_hint && delay > 0.0: yield (self.get_tree ().create_timer (delay), "timeout"); pname = self._prop_name_accuracy (pname);
		# The given property is defined.
		if self.__props__.has (pname):
			# The property is not private.
			if !self.__props__ [pname].private:
				# If the property is write only or read and write mode.
				if self.__props__ [pname].stream == self.PropertyAccessMode.WRITE_ONLY || self.__props__ [pname].stream == self.PropertyAccessMode.BOTH:
					# Checks property activation.
					if self.__props__ [pname].enabled:
						# The target type is not equal to his value.
						if self.__props__ [pname].type == TYPE_INT && typeof (self.__props__ [pname].value) == TYPE_REAL:
							# Converts to integer.
							self.__props__ [pname].value = int (self.__props__ [pname].value);
						# The target type is not equal to his value.
						elif self.__props__ [pname].type == TYPE_REAL && typeof (self.__props__ [pname].value) == TYPE_INT:
							# Converts to float.
							self.__props__ [pname].value = float (self.__props__ [pname].value);
						# The type of the given is the same with his old value.
						if typeof (self.__props__ [pname].value) == typeof (value) && hash (self.__props__ [pname].value) != hash (value):
							# Updates the "__props__" data and run "changed" key configurations on enter tree.
							self.__props__ [pname].value = value; if !Engine.editor_hint and wait: yield (self, "tree_entered");
							# Don't check if property value has been changed on the first game initialisation
							if !wait or Engine.editor_hint: self._prop_value_changed (pname);
							# Some attachments are been detected.
							if self.__props__ [pname].attach != null:
								# Runs constraints for all attached properties.
								for property in self.__props__ [pname].attach: self._run_constraints (self._prop_name_accuracy (property));
							# Runs constraints for all availables properties.
							else: for property in self.__props__: self._run_constraints (self._prop_name_accuracy (property));
				# Otherwise.
				else: self.output (("You cannot change value of {" + pname + "} property."), self.Message.ERROR, self);
			# Error message.
			else: self.output (('{' + pname + "} property is private."), self.Message.ERROR, self);
	# Warning message.
	else: self.output ("The property name mustn't be empty.", self.Message.WARNING, self);

"""
	@Description: Gets value of the given property. This one must be defined on "__props__" property.
	@Parameters:
		String pname: Contains the property name.
		bool dropdown: To return any dropdown value as string format and other value type.
"""
func get_prop (pname: String, dropdown: bool = false):
	# Corrects the given property name.
	pname = str_replace (pname, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' ');
	# If the property name is not empty.
	if not pname.empty ():
		# Getting exactly key value.
		pname = self._prop_name_accuracy (pname);
		# The given property is it defined ?
		if self.__props__.has (pname):
			# The property is not private.
			if !self.__props__ [pname].private:
				# If the property is readonly or read and write mode.
				if self.__props__ [pname].stream == self.PropertyAccessMode.READ_ONLY || self.__props__ [pname].stream == self.PropertyAccessMode.BOTH:
					# Is it enabled ?
					if self.__props__ [pname].enabled:
						# Checks dropdown key presence.
						if dropdown && self.__props__ [pname].hint_string != null:
							# Finds a comma.
							if self.__props__ [pname].hint_string.find (',') != -1:
								# Getting the current value as string format.
								var value = self.__props__ [pname].hint_string.split (',');
								# Correcting selection index.
								value = (value [self.__props__ [pname].value] if self.index_validation (self.__props__ [pname].value, len (value)) else value [0]);
								# Keycode key is it defined ?
								if self.__props__ [pname]._drop_opt != null:
									# For mouse and joystick option.
									if self.__props__ [pname]._drop_opt == self.NaughtyAttributes.MOUSE_CONTROLS\
									|| self.__props__ [pname]._drop_opt == self.NaughtyAttributes.GAMEPAD_CONTROLS:
										# Contains a splited string.
										value = value.split (' '); return int (value [(len (value) - 1)]);
									# Size key is it defined ?
									elif self.__props__ [pname]._drop_opt == self.NaughtyAttributes.DESKTOP_RESOLUTIONS\
									|| self.__props__ [pname]._drop_opt == self.NaughtyAttributes.IPAD_RESOLUTIONS\
									|| self.__props__ [pname]._drop_opt == self.NaughtyAttributes.IPHONE_RESOLUTIONS\
									|| self.__props__ [pname]._drop_opt == self.NaughtyAttributes.ANDROID_RESOLUTIONS:
										# Getting the real resolution value and returns the final result.
										value = value.split (' ') [1].lstrip ('(').rstrip (')').split ('x'); return Vector2 (int (value [0]), int (value [1]));
									# For keyboard option.
									elif self.__props__ [pname]._drop_opt == self.NaughtyAttributes.KEYBOARD_CONTROLS:
										# Getting keyboard keycode.
										value = value.split (' ') [1].lstrip ('(').rstrip (')'); return int (value);
									# For mains system folders.
									elif self.__props__ [pname]._drop_opt == self.NaughtyAttributes.SYSTEM_DIR: return self.get_os_dir (self.__props__ [pname].value);
								# Returns dropdown as string format.
								else: return value;
							# Returns the single value.
							else: return self.__props__ [pname].hint_string;
						# Returns the property value.
						else: return (self.__props__ [pname].value.duplicate (true) if self.__props__ [pname].value is Array\
						|| self.__props__ [pname].value is Dictionary else self.__props__ [pname].value);
					# The game isn't running.
					elif !Engine.editor_hint: return self.get_initialised_type (self.__props__ [pname].value);
					# Runtime mode.
					else: return (self.__props__ [pname].value.duplicate (true) if self.__props__ [pname].value is Array\
					|| self.__props__ [pname].value is Dictionary else self.__props__ [pname].value);
				# Otherwise.
				else: self.output (("You cannot get value of {" + pname + "} property."), self.Message.ERROR, self);
			# Error message.
			else: self.output (('{' + pname + "} property is private."), self.Message.ERROR, self);
	# Warning message.
	else: self.output ("The property name mustn't be empty.", self.Message.WARNING, self);

"""@Description: Returns all properties from "__props__" dictionary.
	@Parameters:
		bool invert: Do you want to reverse the alignment order of the properties ?
"""
func get_properties (invert: bool = false) -> Array:
	# Contains all properties of the module.
	var properties: Array = Array ([]); var index: int = 0;
	# Gets all properties with a certains conditions.
	for prop in self.__props__:
		# Checks property visibility.
		if self.__props__ [prop].visible:
			# Contains the property data.
			var prop_data: Dictionary = Dictionary ({name = prop, hint_string = self.__props__ [prop].hint_string, type = self.__props__ [prop].type,
				usage = self.__props__ [prop].usage, hint = self.__props__ [prop].hint
			});
			# The property index is inside range array.
			if self.is_range (self.__props__ [prop].index, 0, (len (properties) - 1)) || self.__props__ [prop].index == index:
				# Inserts the given property and go to the next index.
				index = self.__props__ [prop].index; properties.insert (index, prop_data); index += 1;
			# Otherwise.
			else:
				# Inserts the given property and go to the next index.
				properties.insert (index, prop_data); index += 1;
	# Returns the final result.
	return self.array_invert (properties) if invert else properties;

"""
	@Description: Adds one or severals property(ies) to a script. Before continuing, you should know that the
		following keys can be manipulated in the use of keys: "showif, disableif, require, changed, min, max,
		button, clone and notification".
		-> String | bool statement: Contains the developer's statement. If you donate a string, then it will
			have to close a condition which is exactly like the declarations you make when you define a
			condition in programming, but with a few differences.
			Example:
				"property1 > 56 || property2 != ?property3 and !property4"
				"method1 () == not option"
				"property5 <= ?method2 () and property4 or !method1 ()"
				"StringProperty == null" => Will check if "StringProperty" in string format is empty ('')
			The question mark means that we wants to fetch the value that another predefined property or method
			returns. The mathematical expressions and parameterizable method calls are not supported in a
			statement. However, if you want to retrieve the value of a key or an index of position contained
			in a dictionary or table you must adopt the nomenclature: "PropertyName.KeyName/PositionIndex".
			Example: "table1.2 > ?table2.0 || dictionary1.2 != ?dictionary3.level".
			However, only use this key if you are handling the following keys: "showif, disableif, clone, require and notification."
		-> Array | Dictionary actions: Contains the different configurations on how actions will be performed
			upon verification of certain conditions. If you give an array, then it should only contain dictionaries
			supporting the same keys as if you pass an ordinary dictionary. If you give a dictionary, then it
			will support the following keys:
				-> String slot: Contains the name of the method or property to be called when the execution conditions are validated.
				-> Variant value: This key is to be used only when the action to be performed is on a property.
					It contains the value assigned to the property in question, if you did not pass a dictionary.
					Otherwise, you will be required to redefine the property with any keys you use to the creation
					of a property via "bind_props ()" method.
				-> Array params: Contains the values ​​of different parameters of the targeted method. This key
					is to be used only when the action to be performed is on a method. The filling of this table,
					must be respected the order of alignment of the parameters of the method in question. No need
					to use this key, the method has no parameter (s) or can take no argument (s). You can get
					property value or method result from parameters with some specials keywords ("?PropertyName"
					or "?MethodName()"). Only on ("value", "changed" and "actions") keys.
				-> String message: Contains the message to be displayed when the execution conditions are validated.
				-> int type = 2: What is the type of the message to printed ?
		-> String callback: Containts a method name (only on "value" and "changed" keys).
	@Parameters:
		Array | Dictionary data: Contains the property data. This dictionary support the following keys:
			-> String Source: What is the property source ?
			-> int index = -1: Control property position hierarchy.
			-> Variant value = null: Contains the value of the property in question. Adapt the value of your
				property according to the type chosen. It is important to do this if you don't want bizzard results.
				If you spend a character string as value, you will be able to retrieve the value of an another
				property or result that a method returns. Example:
					"?property" will set the targeted property to the value of property.
					"?method ()" will affect the targeted property, which returns method.
				You also have the possibility to give a dictionary containing the following keys: "callback" and "params".
			-> int type = 0: Contains the choosed type of the property.
			-> int hint = 0: Contains the property hint.
			-> int usage = 71: Contains the property usage on the editor.
			-> String title: Do you want to change script category name ? If you give a string value, the named category "Script Variables" will set to your value.
			-> Vector2 | Vector3 range: Create a range property with range constraints. If you passed a vector3, the third value containts the range step.
			-> String hint_string: Use this key to group severals values into a dropdown.
			-> bool visible = true: The current will visible on editor inspector ?
			-> bool enabled = true: Control property activation and desactivation.
			-> bool private = false: This property is it private ?
			-> String | PoolStringArray attach: Do you want to attach properties to this property? To make an
			attachment, you must give the name(s) of the property(s) whose behavior depends on this property.
			Example: Let "property1", "property2", "property3" and "property4" be four properties of the script.
			The behaviors of properties 2 and 3 depend on the value of "property1" and that of "property4", that of
			"property3". Thus, the value of the "attach" key of "property1" will target the names of properties 2 and 3;
			that of "property3" will target the "property4" name. Note that using this key optimizes the execution of
			configurations carried out at the level of all the properties of the script. It formally recommended to
			use it to avoid executions outside the predefined domain.
			-> bool duplicate = false: Do you want to repeat the last element value foreach add ? (Work on array only).
			-> String | Dictionary button: Create a boolean property that work as a button.
			-> int stream = 2: Control the property access mode. The possibles values are:
				-> MegaAssets.PropertyAccessMode.READ_ONLY or 0: You can access to this property on read only.
				-> MegaAssets.PropertyAccessMode.WRITE_ONLY or 1: You can access to this property on write only.
				-> MegaAssets.PropertyAccessMode.BOTH or 2: You can access to this property on read and write.
			-> bool saveable = false: Do you want to configure the property to respond to saving and loading script data? Use this property only if your script
				inherits from the class "Saveable" or "Recordable".
			-> String | Dictionary changed: Calls a callback when property value has changed. If you call a method with two or many parameters, the value of the
				first and the second argument will be respectively replaced to property name and his value. Don't forget this detail.
			-> Variant | Dictionary dropdown: Convert the property into an enumerator. You can give a behavior to
				your dropdown think a dictionary that support the following keys:
					-> Variant value: Containts the dropdown value.
					-> int behavior: Which behavior used ? The possbles values are:
						-> MegaAssets.NaughtyAttributes.INPUT_MAP or 0: Get Godot editor input map.
						-> MegaAssets.NaughtyAttributes.SIGNALS or 1: Get current available script signals list.
						-> MegaAssets.NaughtyAttributes.TAGS or 2: Get current node groups.
						-> MegaAssets.NaughtyAttributes.METHODS or 3: Get current available script methods list.
						-> MegaAssets.NaughtyAttributes.TYPES or 4: Get Godot base types list.
						-> MegaAssets.NaughtyAttributes.OPERATORS or 5: Get Godot available operators list.
						-> MegaAssets.NaughtyAttributes.MOUSE_CONTROLS or 6: Get mouse controls. You can also get keycodes by
							enabling "keycodes" key from dropdown configurations. Same thing for gamepad and keyboard controls.
						-> MegaAssets.NaughtyAttributes.GAMEPAD_CONTROLS or 7: Get joystick controls.
						-> MegaAssets.NaughtyAttributes.DESKTOP_RESOLUTIONS or 8: Get all possibles desktop resolutions.
							You can also get sizes by enabling "sizes" key from dropdown configurations. Same thing
							for ipad, iphone and android resolutions.
						-> MegaAssets.NaughtyAttributes.IPAD_RESOLUTIONS or 9: Get all possibles ipad resolutions.
						-> MegaAssets.NaughtyAttributes.IPHONE_RESOLUTIONS or 10: Get all possibles iphone resolutions.
						-> MegaAssets.NaughtyAttributes.ANDROID_RESOLUTIONS or 11: Get all possibles android resolutions.
						-> MegaAssets.NaughtyAttributes.KEYBOARD_CONTROLS or 12: Get keyboard controls.
						-> MegaAssets.NaughtyAttributes.SYSTEM_DIR or 13: Get the list of available paths on the installed system operating system.
							Note that you can also retrieve the paths by setting the "paths" key to true.
						-> MegaAssets.NaughtyAttributes.GAME_CONTROLLERS or 14: Retrieves the list of controls connected to
							the game. Note that this option listened to the commands connected to the computer to
							give in real time orders available. Note that the behaviors of a dropdown list have
							priority over the value of the latter itself.
			-> String | Dictionary showif: Show a property into the editor inspector with a certains conditions.
				Same thing for "disableif" key which takes care of the activation as well as the deactivation of a property.
			-> float | int | Dictionary | Vector2 | Vector3 min: Forces the property's value to stay at the
				minimum imposed. Note that this key is only used if the type of the property is an integer,
				real, 2d vector or 3d vector. If you give a dictionary, then it will support the keys following:
					-> float | int | Vector2 | Vector3 value: Contains the min value.
					-> int index = -1: What value do you want targeted ? Only use this key if the property
						you are manipulating is a 2d vector or 3d vector.
				Same thing for "max" key.
			-> Array | Dictionary clone: Detects presence of duplicates depending on the identifier given to
				it. If you give a table, then this should only contain dictionaries supporting the same keys
				as if you skip an ordinary dictionary. If you give a dictionary, then it will support following
				keys:
					-> Variant id: Contains an identifier of which you want to check for duplications within these values.
					-> int limit = 1: What is the criterion for repeating the values ​​of the chosen identifier ?
			-> Array | Dictionary | String require: Adds some constraints to property value. If you give a string
				of characters, it will be considered as an error message to be displayed when will not change the
				initial value of the property. If you give an array, then this This should only contain
				dictionaries supporting the same keys as if you pass a ordinary dictionary.
			-> Array | Dictionary notification: Listen to them notifications from the editor to then execute
				the configurations made to his espect. If you give an array, then it should only contain
				dictionaries supporting the same keys as if you pass an ordinary dictionary. You can pass a
				dictionary with the following keys:
					-> int what: Which editor notifications will be listen ?
		float delay: What is the death time before adding a module property ?
"""
func bind_prop (data, delay: float = 0.0) -> void:
	# Apply protected visibility to this method.
	if apply_visibility (self.MethodAccessMode.PROTECTED, (self._SCRIPTS_CORE + PoolStringArray ([self.get_class ()]))):
		# Game is running and transforms data parameter into an array.
		if !Engine.editor_hint && delay > 0.0: yield (self.get_tree ().create_timer (delay), "timeout");
		# Adds all given properties.
		data = Array ([data]) if not self.is_array (data) else Array (data); for prop in data:
			# The given data is a dictionary.
			if prop is Dictionary:
				# Corrects the given data.
				var res: Array = self._correct_data (prop); prop = res [0];
				# Contains the user source value.
				if res [1] [0] and prop.source is String:
					# Corrects the given source name.
					prop.source = str_replace (prop.source, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' ');
					# The source is correct.
					if !prop.source.empty () && prop.source != '/':
						# This property is not defined on the script properties.
						if !self.__props__.has (prop.source):
							# Collects all given data into a new dictionary.
							var prop_data: Dictionary = Dictionary ({
								value = prop.value if res [1] [1] else null, _oldvalue = prop.value if res [1] [1] else null,
								type = int (prop.type) if res [1] [2] && is_number (prop.type) else TYPE_NIL,
								hint = int (prop.hint) if res [1] [3] && is_number (prop.hint) else PROPERTY_HINT_NONE,
								usage = int (prop.usage) if res [1] [4] && is_number (prop.usage) else PROPERTY_USAGE_DEFAULT_INTL,
								visible = prop.visible if res [1] [5] && prop.visible is bool else true,
								stream = int (prop.stream) if res [1] [6] && is_number (prop.stream) else 2,
								private = prop.private if res [1] [7] && prop.private is bool else false,
								changed = prop.changed if res [1] [8] else null, button = prop.button if res [1] [11] else null,
								index = int (prop.index) if res [1] [9] && is_number (prop.index) else -1,
								duplicate = prop.duplicate if res [1] [10] && prop.duplicate is bool else false,
								enabled = prop.enabled if res [1] [12] && prop.enabled is bool else true,
								disableif = prop.disableif if res [1] [13] else null, showif = prop.showif if res [1] [14] else null,
								require = prop.require if res [1] [15] else null, min = prop.min if res [1] [16] else null,
								max = prop.max if res [1] [17] else null, clone = prop.clone if res [1] [18] else null,
								notification = prop.notification if res [1] [19] else null, attach = prop.attach if res [1] [20] else null,
								saveable = prop.saveable if res [1] [21] && prop.saveable is bool else false,
								hint_string = prop._hint_str if prop.has ("_hint_str") else null,
								_drop_opt = prop._drop_opt if prop.has ("_drop_opt") && prop._drop_opt is int else null
							}); self.__props__ [prop.source] = prop_data; self.property_list_changed_notify ();
					# Error message.
					else: self.output ("Invalid source key input.", self.Message.ERROR, self);
				# Error message.
				else: self.output ("Missing property source.", self.Message.ERROR, self);

"""
	@Description: Destroys the given properties from his name from "__props__" dictionary.
	@Parameters:
		String | PoolStringArray source: Contains properties names or their source.
		float delay: What is the timeout before destroying properties ?
"""
func destroy_props (source, delay: float = 0.0) -> void:
	# Apply protected visibility to this method.
	if apply_visibility (self.MethodAccessMode.PROTECTED, (self._SCRIPTS_CORE + PoolStringArray ([self.get_class ()]))):
		# Game is running.
		if delay > 0.0 and !Engine.editor_hint: yield (self.get_tree ().create_timer (delay), "timeout");
		# Transforms source parameter into a PoolStringArray.
		source = PoolStringArray (Array ([source]) if not self.is_array (source) else Array (source));
		# Destroys all given properties.
		for src in source:
			# Corrects the given source value.
			src = self._prop_name_accuracy (self.str_replace (src, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' '));
			# The source name isn't empty.
			if not src.empty ():
				# Searches and destroys the given source.
				for prop in self.__props__:
					# The source contains '/' character.
					if src.find ('/') == -1:
						# Destroys this property from the script.
						if src == prop and self.__props__.erase (prop): break;
					# Otherwise.
					elif prop.begins_with (src) && self.__props__.erase (prop): pass;
		# Refreshes script properties data.
		self.property_list_changed_notify (); self._destroy_unusefull_props ();

"""
	@Description: Overrides a property.
	@Parameters:
		Dictionary data: Contains useful data to the new property.
		String prop_name: Contains property name.
		float delay: What is the timeout before redefinition ?
"""
func override_prop (data: Dictionary, prop_name: String, delay: float = 0.0) -> void:
	# Apply protected visibility to this method.
	if apply_visibility (self.MethodAccessMode.PROTECTED, (self._SCRIPTS_CORE + PoolStringArray ([self.get_class ()]))):
		# Game is running.
		if !Engine.editor_hint && delay > 0.0: yield (self.get_tree ().create_timer (delay), "timeout");
		# Corrects the passed property name.
		prop_name = self._prop_name_accuracy (self.str_replace (prop_name, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' '));
		# Checks old property name value.
		if not prop_name.empty ():
			# Checks property name into the module properties data.
			if self.__props__.has (prop_name):
				# Corrects the given data and assigned source.
				var res: Array = self._correct_data (data); data = res [0]; data.source = prop_name if !res [1] [0] else data.source;
				# Corrects the given source.
				data.source = self.str_replace (data.source, PoolStringArray (["\n", "\t", "\a", "\b", "\r", "\v", "\f"]), '').lstrip (' ').rstrip (' ');
				# Checks source value.
				if !data.source.empty () && data.source != '/':
					# The property name changed.
					if data.source != prop_name:
						# Copy all data from the old property and replaces the old property to the new property.
						var prop_data: Dictionary = self.__props__ [prop_name]; if self.__props__.erase (prop_name): self.__props__ [data.source] = prop_data;
					# Configures the given data.
					self.__props__ [data.source].value = data.value if res [1] [1] else self.__props__ [data.source].value;
					self.__props__ [data.source]._oldvalue = data.value if res [1] [1] && typeof (self.__props__ [data.source]._oldvalue)\
						!= typeof (data.value) else self.__props__ [data.source]._oldvalue;
					self.__props__ [data.source].type = int (data.type) if res [1] [2] && is_number (data.type) else self.__props__ [data.source].type;
					self.__props__ [data.source].hint = int (data.hint) if res [1] [3] && is_number (data.hint) else self.__props__ [data.source].hint;
					self.__props__ [data.source].usage = int (data.usage) if res [1] [4] && is_number (data.usage) else self.__props__ [data.source].usage;
					self.__props__ [data.source].visible = data.visible if res [1] [5] && data.visible is bool else self.__props__ [data.source].visible;
					self.__props__ [data.source].stream = int (data.stream) if res [1] [6] && is_number (data.stream) else self.__props__ [data.source].stream;
					self.__props__ [data.source].private = data.private if res [1] [7] && data.private is bool else self.__props__ [data.source].private;
					self.__props__ [data.source].changed = data.changed if res [1] [8] else self.__props__ [data.source].changed;
					self.__props__ [data.source].index = int (data.index) if res [1] [9] && is_number (data.index) else self.__props__ [data.source].index;
					self.__props__ [data.source].duplicate = data.duplicate if res [1] [10] && data.duplicate is bool else self.__props__ [data.source].duplicate;
					self.__props__ [data.source].button = data.button if res [1] [11] else self.__props__ [data.source].button;
					self.__props__ [data.source].enabled = data.enabled if res [1] [12] && data.enabled is bool else self.__props__ [data.source].enabled;
					self.__props__ [data.source].disableif = data.disableif if res [1] [13] else self.__props__ [data.source].disableif;
					self.__props__ [data.source].showif = data.showif if res [1] [14] else self.__props__ [data.source].showif;
					self.__props__ [data.source].require = data.require if res [1] [15] else self.__props__ [data.source].require;
					self.__props__ [data.source].min = data.min if res [1] [16] else self.__props__ [data.source].min;
					self.__props__ [data.source].max = data.max if res [1] [17] else self.__props__ [data.source].max;
					self.__props__ [data.source].clone = data.clone if res [1] [18] else self.__props__ [data.source].clone;
					self.__props__ [data.source].notification = data.notification if res [1] [19] else self.__props__ [data.source].notification;
					self.__props__ [data.source].attach = data.attach if res [1] [20] else self.__props__ [data.source].attach;
					self.__props__ [data.source].saveable = data.saveable if res [1] [21] && data.saveable is bool else self.__props__ [data.source].saveable;
					self.__props__ [data.source].hint_string = data._hint_str if data.has ("_hint_str") else self.__props__ [data.source].hint_string;
					self.__props__ [data.source]._drop_opt = data._drop_opt if data.has ("_drop_opt") else self.__props__ [data.source]._drop_opt;
					# Refreshes script properties data.
					self.property_list_changed_notify ();
				# Error message.
				else: self.output ("Invalid source key input.", self.Message.ERROR, self);
		# Error message.
		else: self.output ("Missing the old property name.", self.Message.ERROR, self);

"""@Description: Checks whether the current module instance is ready to use. Otherwise, an error will be thrown."""
func check_initialization () -> bool:
	# Apply protected visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PROTECTED, (self._SCRIPTS_CORE + PoolStringArray ([self.get_class ()]))):
		# The current module reference is it initialized ?
		if not self._is_initialized:
			# Error message.
			self.output ("This module instance isn't ready.", self.Message.ERROR, self); return false;
		# Otherwise.
		else: return true;
	# Otherwise.
	else: return false;

"""
	@Description: Prints module log. This method uses "Verbose" module property value to work. Useful to make some logs about module treatments.
	@Parameters:
		String message: What is message value that will be printed on editor console ?
"""
func verbose (message: String) -> void:
	# Apply protected visibility to this method.
	if self.apply_visibility (self.MethodAccessMode.PROTECTED, (self._SCRIPTS_CORE + PoolStringArray ([self.get_class ()]))):
		# Getting "Verbose" module property.
		var is_verbose = self.get_prop ("Verbose"); is_verbose = is_verbose if is_verbose is bool else false;
		# Shows a normal message.
		if is_verbose: self.output (message, self.Message.NORMAL, self);
